<?php get_header(); ?>
	<main id="content">
		<div class="inform">
			<h1><?php _e( 'Category', 'simplepuzzle' ); ?> &laquo;<?php single_cat_title(''); ?>&raquo;</h1>
			<?php if ( get_query_var('paged') == '' ) echo category_description(); ?>
		</div>
	<?php while (have_posts()) : the_post(); 

		get_template_part( 'content', 'short' );

	endwhile; ?>

	<?php if(function_exists('avd_the_pagination')) { avd_the_pagination(false); } ?>

	</main> <!-- #content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>