<?php

	global $markup;

	$meta = get_avd_option('single_meta_data');
	$show_all = ( empty($meta) || 'all'==$meta || (is_array($meta) && in_array('all', $meta)) ) ? true : false;
	$show_date = ( is_array($meta) && in_array('date', $meta) ) ? true : false;
	$show_cats = ( is_array($meta) && in_array('categories', $meta) ) ? true : false;
	$show_tags = ( is_array($meta) && in_array('tags', $meta) ) ? true : false;
	$show_comm = ( is_array($meta) && in_array('comments', $meta) ) ? true : false;

?>

	<article <?php post_class(); the_item_markup(); ?>>
	 <?php if (is_single()){?>
			<div class='post_thumbnail'>
				<?php if ( has_post_thumbnail() && get_avd_option('show_thumbnail_onsingle') ) {
					the_post_thumbnail( 'post_fullwidth', 'class=thumbnail' );
				} ?>
				<h1 class="single_title"<?php echo ($markup) ? ' itemprop="headline"' : ''; ?>><?php the_title(); ?></h1>
			</div>
			<?php
			} 
			else{?>
				<h1 class="single_title page-title"<?php echo ($markup) ? ' itemprop="headline"' : ''; ?>><?php the_title(); ?></h1>
		<?php } ?>

			
			<div class="likely-top">
			<!-- <?php echo do_shortcode('[show_likely]'); ?> -->
			</div>


		

		<div class="entry clearfix" <?php if ($markup) { echo "itemprop='articleBody'"; } ?>>
		
	<!-- <?php  add_filter ('the_content', 'append_likely', 1); ?> -->
		
		<?php the_content(); ?>
		</div>

		<?php the_markup_schemaorg(); ?>
	</article> 
