<?php

	$thecontent = get_avd_option('show_thecontent');
	$thecontent = ( 'thecontent' == $thecontent ) ? true : false;
	
	global $markup_lists;

?>

	<article <?php post_class(); the_item_markup( true ); ?>>
			
		<?php if ( has_post_thumbnail() ) {
			$imgsize = get_avd_option('thumbsize'); 
			$imgsize = ( $imgsize ) ? $imgsize : 'medium';
			?>
			<h2 class="review_title"<?php echo ($markup_lists) ? ' itemprop="headline"' : ''; ?>> 
			<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?><div class="both"></div><?php the_post_thumbnail( $imgsize, 'class=thumbnail' ); ?></a>
		</h2>			
						
		<?php } 
		else { ?>
		
		
				<h2 class="review_title"<?php echo ($markup_lists) ? ' itemprop="headline"' : ''; ?>> 
			<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h2>		
		<?php } ?>

		<div class="entry"<?php if ($markup_lists) { echo " itemprop='articleBody'"; } ?>>			
			<?php if ( $thecontent ) {
				the_content(''); 
			} else {
				the_excerpt('');
			} ?>
		</div>	
					
		<div class="button-wrap">
			<a class="more-link" href="<?php the_permalink(); echo ($thecontent) ? '#more-' . get_the_id() : '' ; ?>">
				<?php echo __('Read more', 'simplepuzzle') ?>
			</a>
		</div>


		<?php the_markup_schemaorg( 'short' ); ?>
			
	</article> 
