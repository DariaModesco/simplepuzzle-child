	</div> 
	<!-- #main -->
	<?php if ( is_active_sidebar( 'footerbar' ) ) { ?>
	<aside id="footerbar" class="clearfix">

		<ul class="wlist clearfix">
			<?php dynamic_sidebar( 'footerbar' ); ?>
		</ul>
		
	</aside>
	<?php } ?>


</div>
<!-- .pagewrapper -->

<footer id="footer">
		<?php $copyright_text = get_theme_mod( 'true_footer_copyright_text', __('All rights reserved', 'simplepuzzle') . ' &copy; ' . date('Y') );
		if ( $copyright_text || is_customize_preview() ) { ?>
			<p id="footer_custom"><?php echo $copyright_text; ?></p>
		<?php }

		?>
	<?php wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_class' => 'nav-menu' ) ); ?> <div class="inner">

	</div>
	
</footer>

<div class="mobile-menu">
<ul class='hamburger'>
      <li class='first'></li>
      <li class='second'></li>
      <li class='third'></li>
    </ul>
<?php 

	if (has_nav_menu('left_mobile')) : 
		wp_nav_menu( array(
			'theme_location' => 'left_mobile',
			'container'      => false,
			'items_wrap'     => '<ul id="left_mobile" class="mobmenu on-mobile clearfix">%3$s</ul>'
		) ); 
	else : ?>
		<ul id="left_mobile" class="mobmenu on-mobile clearfix"><?php wp_list_pages('title_li=&depth=2'); ?></ul>
	<?php endif;	

	if (has_nav_menu('right_mobile')) : 
		wp_nav_menu( array(
			'theme_location' => 'right_mobile',
			'container'      => false,
			'items_wrap'     => '<ul id="right_mobile" class="mobmenu on-mobile clearfix">%3$s</ul>'
		) ); 
	else : ?>
		<ul id="right_mobile" class="mobmenu on-mobile clearfix"><?php wp_list_categories('title_li=&depth=2'); ?></ul>
	<?php endif;

?>
</div>

<a id="toTop">&#10148;</a>
</div>

<?php wp_footer(); ?>

</body>
</html>