<?php get_header(); ?>
<?php get_sidebar(); ?>
	<main id="content" class="main-top">

<?php  


/**
 * blog posts on frontpage 
 * ==================================================== */
if ( !is_singular() && have_posts()) : 

	while (have_posts()) : the_post(); 
		get_template_part( 'content', 'short' ); 
	endwhile; 

	avd_the_pagination( false ); 


	
/**
 * static page with post announces
 * ==================================================== */
elseif ( is_singular() && 'magazine' == get_avd_option('home_type') ) : 

		// get_template_part( 'review' ); 
		get_template_part( 'homepage' ); 

	
/**
 * static page with text 
 * ==================================================== */
elseif ( is_singular() ) : 

		get_template_part( 'content', 'full' ); 

endif; ?>
	    <?php $post = get_post($post_id = 4);
    echo '<div class="featured-posts-box grid clearfix"><h2>'.$post->post_title.'</h2>';
    echo '<div>'.$post->post_content.'</div></div><div class="clear"></div>';?>
    </main> 
	<!-- END #content -->
	

<?php get_footer(); ?>