<?php

define( 'APP_NAME', 'simplepuzzle' );
define( 'SMPZ_TEMPL_PATH', get_template_directory() );
define( 'SMPZ_TEMPL_URI', get_template_directory_uri() );

$exclude_posts = array();


/* ==========================================================================
 *  Theme settings
 * ========================================================================== */
if ( ! function_exists( 'simplepuzzle_setup' ) ) :
function simplepuzzle_setup() {

	if ( ! isset( $content_width ) )
		$content_width = 725;

	load_theme_textdomain( 'simplepuzzle', SMPZ_TEMPL_PATH . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );


	add_image_size( 'post_fullwidth', 720, 250, true );
	add_image_size('widget_last_posts', 240, 160, true);

	add_theme_support( 'title-tag' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
	add_filter ('the_content', 'append_likely', 1);
	add_theme_support( 'custom-background', apply_filters( 'simplepuzzle_custom_background_args', array('default-color' => 'ffffff') ) );
	add_theme_support( 'custom-header', array(
		'width'         => 1160,
		'height'        => 130,
	) );
	
	register_nav_menus( array(
  'top'  => __( 'Pages Menu', 'simplepuzzle' ),
  'main' => __( 'Main Menu',  'simplepuzzle' ),
  'footer' => __( 'Меню в футере',  'simplepuzzle' ),
  'left_mobile'  => __( 'Left Mobile Menu',  'simplepuzzle' ),
  'right_mobile' => __( 'Right Mobile Menu',  'simplepuzzle' ),
 ) );
}
endif;
add_action( 'after_setup_theme', 'simplepuzzle_setup', 20 );
/* ========================================================================== */



/* ==========================================================================
 *  Load scripts and styles
 * ========================================================================== */
if ( ! function_exists( 'simplepuzzle_enqueue_style_and_script' ) ) :
function simplepuzzle_enqueue_style_and_script() {
	
	global $opt, $post, $wp_query;

	$template_dir = get_template_directory_uri();
	
	// STYLES
	$link = simplepuzzle_get_theme_fonts();
	if ( $link ){
		wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family='. $link .'&amp;subset=latin,cyrillic', array(), true );
	}
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), true);

	// SCRIPTS
	// wp_deregister_script( 'jquery-core' );
	// wp_register_script( 'jquery-core', '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', false, true);
	// wp_enqueue_script( 'jquery' );


	if ( get_avd_option('show_bigslider') || get_avd_option('show_smallslider') ) {
		wp_enqueue_script( 'bxslider', $template_dir . '/js/bxslider.min.js', array('jquery'), true, true );
	}

	wp_enqueue_script( 'scripts', $template_dir . '/js/functions.js', array('jquery'), true, true );
	
	if ( is_singular() ) {
		$socbtns = get_avd_option('social_share');

		if ( 'share42' == $socbtns )
			wp_enqueue_script( 'share42', $template_dir . '/js/share42.js', array('jquery'), true, true );

		if ( 'yandex' == $socbtns ) {
			wp_enqueue_script( 'yandex-share', '//yastatic.net/share2/share.js', array(), true, true );
		}

		if ( comments_open() && get_option('thread_comments') ) {
			wp_enqueue_script( 'comment-reply', false, true, true );
		}

	}

}
endif;
add_action( 'wp_enqueue_scripts', 'simplepuzzle_enqueue_style_and_script' );
/* ========================================================================== */


/* ==========================================================================
 *  admin area
 * ========================================================================== */
function simplepuzzle_load_admin_style() {
    
	$link = simplepuzzle_get_theme_fonts();
	if ( $link ){
		wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family='. $link .'&amp;subset=latin,cyrillic', array(), true );
		// wp_enqueue_style( 'fonts', '//fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&amp;subset=latin,cyrillic', array(), true );
	}
    add_editor_style( 'css/editor-style.css' );
    
}
add_action( 'admin_enqueue_scripts', 'simplepuzzle_load_admin_style' );
/* ========================================================================== */



/* ==========================================================================
 *  Register widget area
 * ========================================================================== */
function simplepuzzle_widgets_init() {
	
	register_sidebar(array(
		'name' => __('Sidebar', 'simplepuzzle'),
		'id' => 'sidebar',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'simplepuzzle' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<p class="widget-title">',
		'after_title' => '</p>',
	));		
	register_sidebar(array(
		'name' => __('Sidebar-home', 'simplepuzzle'),
		'id' => 'Sidebar-home',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'simplepuzzle' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<p class="widget-title">',
		'after_title' => '</p>',
	));	

	register_sidebar(array(
		'name' => __('Footerbar', 'simplepuzzle'),
		'id' => 'footerbar',
		'description' => __( 'Add widgets here to appear in your sidebar in footer.', 'simplepuzzle' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<p class="widget-title">',
		'after_title' => '</p>',
	));	

	register_sidebar(array(
		'name' => __('Second sidebar', 'simplepuzzle'),
		'id' => 'secondbar',
		'description' => __( 'It is visible on both sidebars layout. Add widgets here to appear in your leftbar.', 'simplepuzzle' ),
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<p class="widget-title">',
		'after_title' => '</p>',
	));	
	 register_sidebar(array(
        'name' => __('Before_Footer', 'simplepuzzle'),
        'id' => 'before_footer',
        'before_widget' => '<div id="before_footer">',
        'after_widget'  => '</div>'
    )); 

}
add_action( 'widgets_init', 'simplepuzzle_widgets_init' );
/* ========================================================================== */



/* ==========================================================================
 *  Add Open Graph meta for singular pages
 * ========================================================================== */
function simplepuzzle_add_social($content) {
	global $post;

	if (is_singular() && get_avd_option('add_social_meta') ) {
		
		$aiod = get_post_meta($post->ID, '_aioseop_description', true);
		$descr = (isset($aiod)) ? esc_html($aiod) : $post->post_excerpt;
		
		$title = get_the_title();
		$url = get_the_permalink();
		$blogname = get_bloginfo('name');
		$img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'thumbnail', false ); 

		echo <<<EOT
		
<!-- BEGIN social meta -->
<meta property="og:type" content="article"/>
<meta property="og:title" content="$title"/>
<meta property="og:description" content="$descr" />
<meta property="og:image" content="$img[0]"/>
<meta property="og:url" content="$url"/>
<meta property="og:site_name" content="$blogname"/>
<link rel="image_src" href="$img[0]" />
<!-- END social meta -->


EOT;
	}
}
add_action( 'wp_head', 'simplepuzzle_add_social' );
/* ========================================================================== */





/* ========================================================================== *
 * default COMMENT callback
 * ========================================================================== */
function simplepuzzle_html5_comment( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
?>
	<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">

			<footer class="comment-meta">
				<div class="comment-author">
					<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
					<b class="fn"><?php echo comment_author_link(); ?></b>
				</div>

				<div class="comment-metadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID, $args ) ); ?>">
						<time datetime="<?php comment_time( 'c' ); ?>">
							<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'simplepuzzle' ), get_comment_date(), get_comment_time() ); ?>
						</time>
					</a>
					<?php edit_comment_link( __( 'Edit', 'simplepuzzle' ), '<span class="edit-link">', '</span>' ); ?>
				</div>

				<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'simplepuzzle' ); ?></p>
				<?php endif; ?>
			</footer>

			<div class="comment-content">
				<?php comment_text(); ?>
			</div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 
							'add_below' => 'div-comment', 
							'depth' => $depth, 
							'max_depth' => $args['max_depth'] 
						)
				)); ?>
			</div>

		</div>

<?php
}

/* ========================================================================== */






/* ==========================================================================
 *  total used SVG icons
 * ========================================================================== */
/*function the_svg_loader(){

	// $icons = get_template_part( 'svg/search', $name ); 
	?>
<!-- SVG icons -->
<svg display="none">
<?php
	
	$social_share = get_avd_option('social_share');
	$soc_pos = get_theme_mod( 'infobar_social', 'fl' );
	$social_share = ( empty($social_share) || 'custom' == $social_share || 'hide' != $soc_pos ) ? true : false;

	include_once "svg/search.svg";
	if ( get_avd_option('tw') || $social_share ) include_once "svg/tw.svg";
	if ( get_avd_option('fb') || $social_share ) include_once "svg/fb.svg";
	if ( get_avd_option('vk') || $social_share ) include_once "svg/vk.svg";
	if ( get_avd_option('ok') || $social_share ) include_once "svg/ok.svg";
	if ( get_avd_option('yt') || $social_share ) include_once "svg/yt.svg";
	if ( get_avd_option('gp') || $social_share ) include_once "svg/gp.svg";

	?>
</svg>
<!-- END SVG icons -->
	<?php

}*/
/* ========================================================================== */





/* ==========================================================================
 *  Include libs
 * ========================================================================== */

// layout functions and filters
	require_once ( SMPZ_TEMPL_PATH . '/inc/layout.php' );

// hooks
	require_once ( SMPZ_TEMPL_PATH . '/inc/hooks.php' );

// custom content functions library
	require_once ( SMPZ_TEMPL_PATH . '/inc/functions.php' );

// admin page options
	require_once ( SMPZ_TEMPL_PATH . '/inc/admin/options.php' );

// Schema.org markup
	require_once ( SMPZ_TEMPL_PATH . '/inc/markup.php' );


if ( is_admin() ) :

	// meta-box for layout control
	require_once ( SMPZ_TEMPL_PATH . '/inc/admin/meta-boxes.php' );

endif;

/* ===================
 * Инициализируем виджеты
 * ===================
 */
include( get_template_directory() . '/widgets/categories.php');
include( get_template_directory() . '/widgets/last-posts.php');
include( get_template_directory() . '/widgets/popular.php');
include( get_template_directory() . '/widgets/last-comments.php');

// remove_filter('the_content', array($this, 'the_content'), 1200);
// add_filter('the_content', array($this, 'the_content'), 10);
remove_action( 'wp_enqueue_scripts', 'advp_add_my_scripts');
add_action('wp_enqueue_scripts', 'advp_add_my_scripts', 25);


remove_action( 'wp_enqueue_scripts', 'likely_enqueue_in_posts');
add_action( 'wp_enqueue_scripts', 'likely_enqueue_in_posts', 25);

remove_action( 'wp_enqueue_scripts', 'likely_enqueue_in_pages');
add_action( 'wp_enqueue_scripts', 'likely_enqueue_in_pages', 25);
