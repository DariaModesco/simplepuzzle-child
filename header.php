<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script><![endif]-->	

<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<?php do_action( 'simplepuzzle_after_open_body>' ); ?>

<?php /*the_svg_loader();*/ ?>

<div class="bodywrap">
<?php do_action( 'simplepuzzle_before_page>' ); ?>

<!-- top bar -->
<!-- Выключаем инфобар-->
<?php //if ( $ib = get_theme_mod('true_infobar',true) || is_customize_preview() ) : ?>
<?php if ( !$ib = get_theme_mod('true_infobar',true) ) : ?>
<div class="infobar">
	<div class="inner clearfix">

		<?php $soc_pos = get_theme_mod( 'infobar_social', 'fl' );
		if ( 'hide'!=$soc_pos || is_customize_preview() ) : ?>
			<div class="top-social<?php echo ' '.$soc_pos; ?>">
			<?php 
			if ( $surl = get_avd_option('tw') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#tw" /></svg></a><?php
			}
			if ( $surl = get_avd_option('fb') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#fb" /></svg></a><?php
			}
			if ( $surl = get_avd_option('vk') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#vk" /></svg></a><?php
			}
			if ( $surl = get_avd_option('ok') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#ok" /></svg></a><?php
			}
			if ( $surl = get_avd_option('yt') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#yt" /></svg></a><?php
			}
			if ( $surl = get_avd_option('gp') ) { ?><a href="<?php echo $surl; ?>" class="soc-link">
				<svg class="soc-item"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#gp" /></svg></a><?php
			}
			?>
			</div>
		<?php endif; ?>	

		<div class="on-mobile mobbar clearfix">
			<?php
				$navloc = get_nav_menu_locations();
				$menu_left  = ( array_key_exists('left_mobile', $navloc ) )  ? get_term( $navloc['left_mobile'], 'nav_menu' )  : false;
				$menu_right = ( array_key_exists('right_mobile', $navloc ) ) ? get_term( $navloc['right_mobile'], 'nav_menu' ) : false;
				$ml_title = ( $menu_left && !is_wp_error($menu_left) )  ? $menu_left->name : __( 'Menu', 'simplepuzzle' );
				$mr_title = ( $menu_right && !is_wp_error($menu_right) ) ? $menu_right->name : __( 'Categories', 'simplepuzzle' );
			?>	
			<a href="#left_mobile" class="mm-button mm-btn-left"><?php echo $ml_title; ?></a>
			<a href="#right_mobile" class="mm-button mm-btn-right"><?php echo $mr_title; ?></a>
			<a href="#topsearch" class="mm-button mobile-search">
				<svg class="search-button"><use xlink:href="<?php echo SMPZ_TEMPL_URI; ?>/svg/social.svg#search"/></svg>
			</a>
		</div>

		<?php $form_pos = get_theme_mod( 'infobar_search', 'fr' );
		if ( 'hide' != $form_pos || is_customize_preview() ) : ?>
		<div id="topsearch" class="mobmenu<?php echo ' '.$form_pos; ?>">
			<?php get_search_form(); ?>
		</div>
		<?php endif; ?>	

		<?php $show_mess = get_theme_mod('infobar_message', 'hide');
		if ( 'hide' != $show_mess || is_customize_preview() ) : ?>
		<div class="top-message<?php echo ' '. $show_mess; ?>">
			<?php echo get_theme_mod('infobar_message_text'); ?>
		</div>
		<?php endif; ?>	

	</div>
</div>
<?php endif; ?>	

<!-- BEGIN header -->
<header id="header">
	<div class="inner grid">
		
		<div class="site-logo<?php if ( get_theme_mod('show_pages_menu', true ) || is_customize_preview() ) { echo ' col4'; } ?>">
			<?php $logo = get_bloginfo('name');
			if ( 'image' == get_avd_option('logotype') ) {
				$logo_src = get_avd_option('logo_image');
				$logo_src = ( !empty($logo_src) ) ? $logo_src : get_template_directory_uri() . '/img/logo.png'; 
				$logo = '<img src="'. $logo_src .'" alt="'. $logo .'" id="logo" class="sitelogo-img">';
			} else {
				$logo = '<span class="sitelogo-txt">'. get_bloginfo('name') .'</span>';
			}
			if ( is_front_page() ) : ?>
				<p class="logo small-logo"><?php echo $logo; ?></p>
			<?php else : ?>
				<a href="<?php echo home_url(); ?>" class="logo small-logo"><?php echo $logo; ?></a>
			<?php endif; ?>

			<?php if ( get_avd_option('showsitedesc') || is_customize_preview() ) : ?>
				<p class="sitedescription<?php echo ( !get_avd_option('showsitedesc') ) ? ' hide' : ''; ?>"><?php bloginfo('description'); ?></p>
			<?php endif; ?>
		</div>

		<?php if ( get_theme_mod('show_pages_menu', true ) || is_customize_preview() ) : ?>
		<div class="pages-menu col8">
			<?php if (has_nav_menu('top')) : 
				wp_nav_menu( array(
					'theme_location' => 'top',
					'container'      => false,
					'items_wrap'     => '<ul  id="top-pages-menu" class="top-pages on-desktop clearfix">%3$s</ul>'
				) ); 
			else : ?>
				<ul id="top-pages-menu" class="top-pages on-desktop clearfix"><?php wp_list_pages('title_li=&depth=2'); ?> </ul>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<?php 
		$himg = get_header_image();
		$show_himg = get_theme_mod('bgimg_header');
		if ( 'image' == $show_himg && $himg ) { ?>
			<img alt="header image" src="<?php echo $himg; ?>" class="header-img">
		<?php } ?>

	</div>
</header>
<!-- END header -->	


<!-- page -->
<div class="pagewrapper inner">

	<!-- BEGIN content -->
	<div id="main" class="clearfix">

	
	<?php if (has_nav_menu('main')) : 
		wp_nav_menu( array(
			'theme_location' => 'main',
			'container'      => false,
			'items_wrap'     => '<ul id="main-menu" class="main-menu clearfix">%3$s</ul>'
		) ); 
	endif;  


get_template_part( 'slider' ); 
get_sidebar( 'second' ); 
