<?php 

$datetime_format = get_option('date_format'); // . ' ' . get_option('time_format');

global $exclude_posts;

/** CATEGORY annonces anf SMALL SLIDER
 * ============================================================== */


if ( get_avd_option('show_bigcat') ) : 
$slidersmall = get_avd_option('show_smallslider'); ?>
<div class="featured-posts-box clearfix <?php echo (!$slidersmall) ? 'whitout-slider ' : ''; ?>">
	<div class="featured-posts left-wrap-two clearfix">

		<?php 
		$cat_id = get_avd_option('home_cat4');
		$cat_pcnt = get_avd_option('home_cat4_cnt');
		$catbigposts = wp_get_recent_posts( array(
					'category' => intval( $cat_id ),
					'numberposts' => intval( $cat_pcnt ),
					'post_status' => 'publish',
					'exclude' => $exclude_posts,
				)); 

		if ( $catbigposts ): $i=0; ?>

		<div class="featured-posts-title">
			<p class="widget-title"><a href="<?php echo get_category_link( $cid ); ?>" class="category-link"><?php echo get_cat_name( $cat_id ); ?></a></p>
			<a class="featured-posts-link" href="<?php echo get_category_link( $cat_id ); ?>"></a>
		</div>
		<div class="">

			<?php foreach ( $catbigposts as $id => $bigimg ) { $i++; ?>
				<div class="small-post small-post-bigimg">
					<a class="anoncethumb" href="<?php echo get_permalink( $bigimg['ID'] ); ?>">
					<?php 

					?>
					<?php if ( has_post_thumbnail($bigimg['ID']) && get_avd_option('show_thumbnail_onsingle') ) {

						echo '<img width="720" height="250" src="'.get_the_post_thumbnail_url( $bigimg['ID'], 'post_fullwidth' ).'" class="thumbnail wp-post-image" alt="'.$bigimg['post_title'].'"/>'; } 

					?>

				<p class="single_title"<?php echo ($markup) ? ' itemprop="headline"' : ''; ?>><?php echo $bigimg['post_title']; ?></p>
					</a>
				</div>
			<?php } ?>

		</div>
		<?php endif; ?>

	</div>


	<?php if ( $slidersmall ) : ?>
	<div class="wrap-second-sl">

		<ul class="second-slider">
			<?php $small_slides = get_avd_option('smallslider');
			foreach ($small_slides as $i => $item) { ?>

				<li class="small-slider-item">
					<img src="<?php echo $item['url']; ?>"alt="<?php echo $item['title']; ?>"/>
					<div class="inside-info">
						<a href="<?php echo $item['link']; ?>"><?php echo $item['title']; ?></a>
					</div>
				</li>

			<?php } ?>
		</ul>

	</div>
	<?php endif; ?>


</div>
<?php endif; 
/** TOP 3 CATEGORIES
 * ============================================================== */

if ( get_avd_option('show_topcat') )  : ?>
<div class="featured-posts-box grid clearfix">

	<?php 
	$cats = array(
		get_avd_option( 'home_cat1' ) => get_avd_option('home_cat1_cnt'),
		get_avd_option( 'home_cat2' ) => get_avd_option('home_cat2_cnt'),
		// get_avd_option( 'home_cat3' ) => get_avd_option('home_cat3_cnt'),
	);

	foreach ($cats as $cid => $cnt) : 
		$posts = wp_get_recent_posts( array(
			'category' => intval( $cid ),
			'numberposts' => intval( $cnt ),
			'post_status' => 'publish',
			'exclude' => $exclude_posts,
		)); 

		if ( empty($posts) ) {
			continue;
		}

	?>
	<div class="featured-cat-wrapper clearfix">

		<div class="featured-posts-title">
			<p class="widget-title"><a href="<?php echo get_category_link( $cid ); ?>" class="category-link"><?php echo get_cat_name( $cid ); ?></a></p>
			<a class="featured-posts-link" href="<?php echo get_category_link( $cid ); ?>"></a>
		</div>

		<div class="featured-posts clearfix">

		<?php foreach ($posts as $id => $postitem) { ?>

			<div class="small-post the-same-post clearfix">
				<a class="anoncethumb"href="<?php echo get_permalink( $postitem['ID'] ); ?>">
					<?php if (has_post_thumbnail( $postitem['ID'] )) { ?>
						<span class="wrap-img"><?php echo get_the_post_thumbnail( $postitem['ID'], 'thumbnail', 'class=bgc-trans' ); ?></span>
					<?php } ?>
					<?php echo $postitem['post_title']; ?>
				</a>
				<div class="entry-meta">
					<span class="date"><?php echo mysql2date( $datetime_format, $postitem['post_date']); ?></span>
				</div>
			</div>
		<?php } ?>

		</div>
	</div>
	
	<?php endforeach; ?>

</div>
<div class="featured-posts-box grid clearfix" style="margin: 0">

	<?php 
	$cats = array(
		// get_avd_option( 'home_cat3' ) => get_avd_option('home_cat3_cnt'),
		// get_avd_option( 'home_cat3' ) => get_avd_option('home_cat3_cnt'),
	);

	foreach ($cats as $cid => $cnt) : 
		$posts = wp_get_recent_posts( array(
			'category' => intval( $cid ),
			'numberposts' => intval( $cnt ),
			'post_status' => 'publish',
			'exclude' => $exclude_posts,
		)); 

		if ( empty($posts) ) {
			continue;
		}

	?>
	<div class="featured-cat-wrapper clearfix">

		<div class="featured-posts-title">
			<p class="widget-title"><a href="<?php echo get_category_link( $cid ); ?>" class="category-link"><?php echo get_cat_name( $cid ); ?></a></p>
			<a class="featured-posts-link" href="<?php echo get_category_link( $cid ); ?>"></a>
		</div>

		<div class="featured-posts clearfix">

		<?php foreach ($posts as $id => $postitem) { ?>

			<div class="small-post the-same-post clearfix">
				<a class="anoncethumb"href="<?php echo get_permalink( $postitem['ID'] ); ?>">
					<?php if (has_post_thumbnail( $postitem['ID'] )) { ?>
						<span class="wrap-img"><?php echo get_the_post_thumbnail( $postitem['ID'], 'thumbnail', 'class=bgc-trans' ); ?></span>
					<?php } ?>
					<?php echo $postitem['post_title']; ?>
				</a>
				<div class="entry-meta">
					<span class="date"><?php echo mysql2date( $datetime_format, $postitem['post_date']); ?></span>
				</div>
			</div>
		<?php } ?>

		</div>

	</div>
	
	<?php endforeach; ?>

</div>
<?php endif; 




/** RECENT POSTS
 * ============================================================== */
if ( get_avd_option('show_recent_posts') ) :

	$title = get_avd_option('show_recent_posts_title');
	$title = ( !empty($title) ) ? $title : __( 'Recent posts', 'simplepuzzle' );

	$recent_posts = wp_get_recent_posts( array(
						'meta_key' => '_postviews',
						'order' => 'DESC',
						'orderby' => 'meta_value_num',
						'numberposts' => 6,
						'post_status' => 'publish',
						'exclude' => $exclude_posts,
					));  

	if ( $recent_posts ): ?>

		<div class="featured-posts-box clearfix">
			<div class="featured-posts-title">
				<p class="widget-title"><?php echo $title; ?></p>
			</div>
			<div class="featured-posts clearfix">

			<?php 

			$iter = 0;
			foreach ($recent_posts as $id => $item) {
// var_dump($item);
				$iter++; 
				if ( 1==$iter) echo '<div class="little-wrapper clearfix">'; 
				if ( 5==$iter) echo '</div><div class="big-wrapper clearfix">';  
				$img_size = ( $iter > 4) ? 'medium' : 'thumbnail'; 

				$big_class = ( $iter > 4 ) ? ' small-post-bigimg' : '';

				?>
		
				<div class="small-post<?php echo $big_class; ?> clearfix">
				<!-- <div class="the-same-post"> -->
					<a class="anoncethumb" href="<?php echo get_permalink( $item['ID'] ); ?>">
						<?php 
						if (has_post_thumbnail( $item['ID'] )) { ?>
							<span class="wrap-img"><?php echo get_the_post_thumbnail( $item['ID'], $img_size ); ?></span>
						<?php } 
						// if ( $iter > 4 ) {
							// echo '<a class="bititle" href="'. get_permalink( $item['ID'] ) . '">'.$item['post_title'].'</a>';
						// } else {
							echo $item['post_title'];
						// } 
						?>
					</a>
					<div class="entry-meta">
						<span class="date"><?php echo mysql2date( $datetime_format, $item['post_date']); ?></span>
					</div>
					<?php if ( $iter > 4 ) {
						// $content_full = strip_tags($item['post_content']);
						// $content_excerpt = substr($content_full, 0, strpos(wordwrap($content_full, 500), "\n"));
						$content_excerpt = wp_trim_words( strip_tags($item['post_content']), 30 );
						echo '<p class="theexcerpt">'. $content_excerpt .'...</p>';
					} ?>
				</div>
			<?php } ?>	
					
			</div>
		</div>

	<?php endif;
    

endif; 