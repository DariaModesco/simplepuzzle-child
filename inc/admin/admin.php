<?php


/**
 *
 * WP Puzzle ADMIN Class
 *
 */
class WPPuzzleAdmin
{

    /**
     * @var mixed
     */
    private $app_name;

    /**
     * @var string URL of the server
     */
    private $APIurl = "http://wp-puzzle.com/api/";

    /**
     * @var string URL for changelog
     */
    private $changelogURL = 'http://wp-puzzle.com/changelog/';

    /**
     * @var mixed slug
     */
    private $slug;

    /**
     * @var mixed
     */
    private static $option_name;

    /**
     * @var mixed
     */
    public static $activate_name;

    /**
     * @var mixed
     */
    public static $access;

    /**
     * @var array
     */
    private $plugin_data = array();

    /**
     * @var mixed
     */
    private $is_plugin;

    /**
     * @var mixed
     */
    private static $is_activated;

    /**
     * @var mixed
     */
    public $response;

    /**
     * @param $app_name
     * @param $is_plugin
     */
    public function __construct( $app_name, $is_plugin )
    {

        $this->app_name       = $app_name;
        self::$option_name    = $app_name.'_check';
        self::$activate_name  = $app_name.'_activate';

        if (get_option(self::$activate_name) === false) {
            self::$is_activated = 'not_activated';
        } else {
            self::$is_activated = get_option(self::$activate_name);
        }

        $myname  = str_replace( array('http://','https://'), '', site_url() );
        $len = strlen($myname);
        $myname  = ( '/' == $myname[ $len -1 ]) ? substr( $myname, 0, $len-1) : $myname;
        self::$access  = md5( $app_name . $myname );

        include ABSPATH.WPINC.'/version.php';
        require_once ABSPATH.'wp-admin/includes/plugin.php';

        $this->is_plugin = $is_plugin;

        if ($is_plugin) {

            $this->plugin_data = get_plugin_data(str_replace('/admin', '', plugin_dir_path(__FILE__)).$this->app_name.'.php');

            add_filter('pre_set_site_transient_update_plugins', array($this, 'checkForUpdates'), 10, 1);
            add_action('install_plugins_pre_plugin-information', array($this, 'overrideUpdateInformation'), 1);

        } else {

            $this->plugin_data['Version'] = wp_get_theme()->get('Version');
            add_filter('pre_set_site_transient_update_themes', array($this, 'checkForUpdates'), 10, 1);

        }

        add_action('init', array($this, 'check_plugin'));

    }

    /**
     * @param  $value
     * @return mixed
     */
    public function checkForUpdates($value)
    {

        $options = array(
            'timeout'    => ((defined('DOING_CRON') && DOING_CRON) ? 30 : 3),
            'user-agent' => 'WordPress/WPuzzleAdmin URL='. get_bloginfo('url') .' ADMIN=' . get_bloginfo('admin_email'),
            'body'       => array(
                'version'   => $this->plugin_data['Version'],
                'plugin'    => $this->app_name,
                'access'    => self::$access,
                //'session'   => $session,
                'theme'     => $this->is_plugin,
                'activated' => self::$is_activated,
            ),
        );

        $raw_response = wp_remote_post($this->APIurl, $options);

        if (is_wp_error($raw_response) || 200 != wp_remote_retrieve_response_code($raw_response)) {
            return $value;
        }

        $response = maybe_unserialize(wp_remote_retrieve_body($raw_response));

        if (empty($response)) {
            return $value;
        }

        // process server returned data
        if ($this->is_plugin) {
            $value->response[$this->app_name.'/'.$this->app_name.'.php'] = $response;
        } else {
            $value->response[$this->app_name] = $response;
        }

        return $value;
    }

    /**
     * @return null
     */
    public function overrideUpdateInformation()
    {
        /*if (wp_unslash($_REQUEST['plugin']) !== $this->slug) {
            return;
        }*/

        wp_redirect( $this->changelogURL . '?appname='. $this->$app_name );
        exit;
    }

    private static function checked_today()
    {
        if (null == get_option(self::$option_name)) {
            update_option(self::$option_name, date("Y-m-d"));
            update_option(self::$activate_name, 'not_activated');
            return false;
        } else {
            $check_date = get_option(self::$option_name);
            $today      = date("Y-m-d");
            $ret        = $check_date && ( strtotime($today) > strtotime($check_date) );
            if ($ret) {
                update_option(self::$option_name, date("Y-m-d"));
                return false;
            } else {
                return true;
            }

        }
    }

    /**
     * @return mixed
     */
    public function check_plugin()
    {
        if ( ! self::checked_today()) {

            $options = array(
                'timeout'    => ((defined('DOING_CRON') && DOING_CRON) ? 30 : 3),
                'user-agent' => 'WordPress/WPuzzleAdmin URL='. get_bloginfo('url') .' ADMIN=' . get_bloginfo('admin_email'),
                'body'       => array(
                    'version'   => $this->plugin_data['Version'],
                    'plugin'    => $this->app_name,
                    'access'    => self::$access,
                    'status'    => 'license',
                    //'session'   => $session,
                    'theme'     => $this->is_plugin,
                    'activated' => self::$is_activated,
                ),
            );

            $raw_response = wp_remote_post($this->APIurl, $options);
            $response     = maybe_unserialize(wp_remote_retrieve_body($raw_response));
            
            if ( !empty($response) ) {
                update_option(self::$activate_name, 'activated');
            } else {
                update_option(self::$activate_name, 'forbidden');
            }

            return $response;
        } else {
            return get_option(self::$activate_name);
        }
    }

}



