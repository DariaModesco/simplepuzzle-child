<?php
/**
 * Created by PhpStorm.
 * User: Владимир
 * Date: 03.03.2016
 * Time: 14:25
 */

//define('APP_NAME','fashionista');
define('APP_NAME_OPTIONS','theme_options_'. APP_NAME);
define('APP_NAME_MODS','theme_mods_'. APP_NAME);


class WPPZ_Importer{

    private $file_contents = array();

    private $file;

    private $defaults = array();

    public function __construct($action,$file=null,$defaults = null)
    {

        switch($action){
            case 'export':
                $this->export();
                break;
            case 'import':
                $this->file = $file;
                $this->import();
                break;
            case 'reset':
                $this->defaults = $defaults;
                $this->reset();
                break;
        }
    }

    private function import(){
        $theme_options = json_decode(file_get_contents($_FILES['my_file_upload']['tmp_name']),true);
        //var_dump($theme_options);
        update_option(APP_NAME_OPTIONS,$theme_options[APP_NAME_OPTIONS]);
        update_option(APP_NAME_MODS,$theme_options[APP_NAME_MODS]);
    }

    private function export(){
        $theme_options[APP_NAME_OPTIONS] = get_option(APP_NAME_OPTIONS);
        $theme_options[APP_NAME_MODS] = get_option(APP_NAME_MODS);

            header('Content-Description: File Transfer');
            header('Content-Type: application/json');
            header('Content-Disposition: attachment; filename='. APP_NAME .'_options.json');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            echo json_encode($theme_options);
            die();
    }

    private function reset()
    {
        update_option(APP_NAME_MODS,'');
        update_option(APP_NAME_OPTIONS,$this->defaults);
    }



}
