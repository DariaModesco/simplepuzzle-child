<?php


/* ======================================================================== *
 * 
 * Add Meta Box
 * 
 * ======================================================================== */
function simplepuzzle_add_custom_box() {
	// Adding layout meta box for page
	add_meta_box( 'page-layout', __( 'Select Layout', 'simplepuzzle' ), 'simplepuzzle_page_layout', 'page', 'side', 'default' ); 
	// Adding layout meta box for
	add_meta_box( 'page-layout', __( 'Select Layout', 'simplepuzzle' ), 'simplepuzzle_page_layout', 'post', 'side', 'default' ); 
}
 add_action( 'add_meta_boxes', 'simplepuzzle_add_custom_box' );
/* ======================================================================== */




/* ======================================================================== */
global $page_layout;
$page_layout = array(
	'default-layout' 	=> array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'default',
		'label' 		=> __( 'Default', 'simplepuzzle' )
	),
	'rightbar' 	=> array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'rightbar',
		'label' 		=> __( 'Rightbar', 'simplepuzzle' )
	),
	'leftbar' 	=> array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'leftbar',
		'label' 		=> __( 'Leftbar', 'simplepuzzle' )
	),
	'both' 	=> array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'both',
		'label' 		=> __( 'Left & Right sidebar', 'simplepuzzle' )
	),
	'full' => array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'full',
		'label' 		=> __( 'Fullwidth Content', 'simplepuzzle' )
	),
	'center' => array(
		'id'			=> 'simplepuzzle_page_layout',
		'value' 		=> 'center',
		'label' 		=> __( 'Centered Content', 'simplepuzzle' )
	)
);
/* ======================================================================== */
	


/* ========================================================================
 *
 * Displays metabox to for select layout option
 *
 * ======================================================================== */
function simplepuzzle_page_layout() {  
	global $page_layout, $post; 

	// Use nonce for verification  
	wp_nonce_field( basename( __FILE__ ), 'custom_meta_box_nonce' );

	foreach ($page_layout as $field) {  
		$layout_meta = get_post_meta( $post->ID, $field['id'], true );
		if( empty( $layout_meta ) ) { $layout_meta = 'default'; }
		?>
			<label class="post-format-icon">
				<input class="post-format" type="radio" name="<?php echo $field['id']; ?>" value="<?php echo $field['value']; ?>" <?php checked( $field['value'], $layout_meta ); ?>/>
			<?php echo $field['label']; ?></label><br/>
		<?php
	}
}
/* ======================================================================== */



/* ========================================================================
 *
 * save the custom metabox data
 *
 * ======================================================================== */
function simplepuzzle_save_custom_meta( $post_id ) { 
	global $page_layout, $post; 
	
	// Verify the nonce before proceeding.
    if ( !isset( $_POST[ 'custom_meta_box_nonce' ] ) || !wp_verify_nonce( $_POST[ 'custom_meta_box_nonce' ], basename( __FILE__ ) ) )
      return;
		
	// Stop WP from clearing custom fields on autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE)  
      return;
		
	if ('page' == $_POST['post_type']) {  
      if (!current_user_can( 'edit_page', $post_id ) )  
         return $post_id;  
   } 
   elseif (!current_user_can( 'edit_post', $post_id ) ) {  
      return $post_id;  
   }  
	
	foreach ($page_layout as $field) {  
		//Execute this saving function
		$old = get_post_meta( $post_id, $field['id'], true); 
		$new = isset( $_POST[$field['id']] ) ? $_POST[$field['id']] : 'default';
		if ($new && $new != $old) {  
			update_post_meta($post_id, $field['id'], $new);  
		} elseif ('' == $new && $old) {  
			delete_post_meta($post_id, $field['id'], $old);  
		} 
	} // end foreach   
}
add_action('pre_post_update', 'simplepuzzle_save_custom_meta'); 
/* ======================================================================== */

