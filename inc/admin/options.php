<?php

$themename = wp_get_theme().'';

if ( !defined('APP_NAME') ) {
	define( 'APP_NAME', get_template() );
}
define( 'SMPZ_OPTION_NAME', 'theme_options_' . APP_NAME );


// add library with controls
require_once ( SMPZ_TEMPL_PATH . '/inc/admin/options/controls.php' );



/* ==========================================================================
 * 	customize get_option for theme options
 * ========================================================================== */
if ( ! function_exists( 'get_avd_option' ) ) :
function get_avd_option( $key, $default = '' ) {
//	global $optionname;

    $cache = wp_cache_get( 'avd_'. SMPZ_OPTION_NAME );
    if ( $cache ) {
		return ( isset( $cache[$key] ) ) 
			? $cache[$key] 
			: false;
    }

	$opt = get_option( SMPZ_OPTION_NAME );
    wp_cache_add( 'avd_'. SMPZ_OPTION_NAME, $opt  );

    // return ( isset($opt[$key]) ) ? $opt[$key] : false;
    if ( isset( $opt[$key] ) ) {
    	return $opt[$key];
    } else {
    	return ( !empty($default) ) 
    		? $default
    		: false;
    }

}
endif;
/* ============================================================================= */


function simplepuzzle_get_options_list( $return_defaults = false ) {


	$cats    = get_categories();
	$catlist = array();
	foreach ( $cats as $id => $value ) {
		$catlist[ $value->term_id ] = $value->cat_name;
	}
	$catlist_ids = array_keys( $catlist );


	/* =============================================================================
	 * theme options array with defaults
	 * ============================================================================= */

	$avd_options = array(

		// GENERAL
		// .........................................................................
		array(
			"name" => "<a href='#tabgeneral' class='tab-title general' title='" . __( 'General theme options', 'simplepuzzle' ) . "'><span class='dashicons dashicons-admin-generic'></span><b>" . __( 'General', 'simplepuzzle' ) . "</b></a>",
			"id"   => "general",
			"type" => "section"
		),

		array(
			"name"    => __( "Featured images size", 'simplepuzzle' ),
			"desc"    => __( "Size images for post lists (blog and archives)", 'simplepuzzle' ),
			"id"      => "thumbsize",
			"std"     => "medium",
			"type"    => "radiogroup",
			"options" => array(
				'thumbnail' => __( "Thumbnail", 'simplepuzzle' ),
				'medium'    => __( "Medium", 'simplepuzzle' ),
				'large'     => __( "Large", 'simplepuzzle' ),
			)
		),


		array(
			"name"    => __( "Posts announces", 'simplepuzzle' ),
			"desc"    => __( "How show text announces in posts list", 'simplepuzzle' ),
			"id"      => "show_thecontent",
			"std"     => "excerpt",
			"type"    => "radiogroup",
			"options" => array(
				'excerpt'    => __( "Excerpt", 'simplepuzzle' ),
				'thecontent' => __( "Content", 'simplepuzzle' ),
			)
		),


		array(
			"name"    => __( "Default layout", 'simplepuzzle' ),
			"desc"    => __( "Default layout for all pages", 'simplepuzzle' ),
			"id"      => "layout_default",
			"std"     => "rightbar",
			"type"    => "select",
			"options" => array(
				'rightbar' => __( "Rightbar", 'simplepuzzle' ),
				'leftbar'  => __( "Leftbar", 'simplepuzzle' ),
				'both'     => __( "Left & Right sidebar", 'simplepuzzle' ),
				'full'     => __( "Fullwidth Content", 'simplepuzzle' ),
				'center'   => __( "Centered Content", 'simplepuzzle' )
			)
		),

		array(
			"name"    => __( "Home layout", 'simplepuzzle' ),
			"desc"    => __( "Index page with Blog posts", 'simplepuzzle' ),
			"id"      => "layout_home",
			"std"     => "rightbar",
			"type"    => "select",
			"options" => array(
				'rightbar' => __( "Rightbar", 'simplepuzzle' ),
				'leftbar'  => __( "Leftbar", 'simplepuzzle' ),
				'both'     => __( "Left & Right sidebar", 'simplepuzzle' ),
				'full'     => __( "Fullwidth Content", 'simplepuzzle' ),
				'center'   => __( "Centered Content", 'simplepuzzle' )
			)
		),

		array(
			"name"    => __( "Post layout", 'simplepuzzle' ),
			"desc"    => "",
			"id"      => "layout_post",
			"std"     => "rightbar",
			"type"    => "select",
			"options" => array(
				'rightbar' => __( "Rightbar", 'simplepuzzle' ),
				'leftbar'  => __( "Leftbar", 'simplepuzzle' ),
				'both'     => __( "Left & Right sidebar", 'simplepuzzle' ),
				'full'     => __( "Fullwidth Content", 'simplepuzzle' ),
				'center'   => __( "Centered Content", 'simplepuzzle' )
			)
		),


		array(
			"name"    => __( "Pages layout", 'simplepuzzle' ),
			"desc"    => "",
			"id"      => "layout_page",
			"std"     => "center",
			"type"    => "select",
			"options" => array(
				'rightbar' => __( "Rightbar", 'simplepuzzle' ),
				'leftbar'  => __( "Leftbar", 'simplepuzzle' ),
				'both'     => __( "Left & Right sidebar", 'simplepuzzle' ),
				'full'     => __( "Fullwidth Content", 'simplepuzzle' ),
				'center'   => __( "Centered Content", 'simplepuzzle' )
			)
		),


		// HEADER
		// .........................................................................
		array(
			"name" => "<a href='#tabheader' class='tab-title header' title='" . __( 'Site header options', 'simplepuzzle' ) . "'><span class='dashicons dashicons-archive'></span><b>" . __( 'Header', 'simplepuzzle' ) . "</b></a>",
			"id"   => "header",
			"type" => "section"
		),


		array(
			"name"    => __( "Logo view", 'simplepuzzle' ),
			"desc"    => __( "What do you want to display?", 'simplepuzzle' ),
			"id"      => "logotype",
			"std"     => "text",
			"type"    => "select",
			"options" => array(
				'image' => __( "Image", 'simplepuzzle' ),
				'text'  => __( "Site title", 'simplepuzzle' ),
			)
		),

		array(
			"name" => __( "Logo image", 'simplepuzzle' ),
			"desc" => __( "Set up logo image (works width 'image' logo type)", 'simplepuzzle' ),
			"id"   => "logo_image",
			"std"  => get_template_directory_uri() . "/img/logo.png",
			"type" => "image",
		),

		array(
			"name" => __( "Site descriptions", 'simplepuzzle' ),
			"desc" => __( "Show site description under logo?", 'simplepuzzle' ),
			"id"   => "showsitedesc",
			"std"  => "0",
			"type" => "checkbox",
		),


		// HOME page
		// .........................................................................
		array(
			"name" => "<a href='#tabhomepage' class='tab-title Home page' title='" . __( 'Site home page options', 'simplepuzzle' ) . "'><span class='dashicons dashicons-admin-home'></span><b>" . __( 'Home page', 'simplepuzzle' ) . "</b></a>",
			"id"   => "homepage",
			"type" => "section"
		),

		array(
			"name"    => __( "Static front page content", 'simplepuzzle' ),
			"desc"    => __( "What do you want to display on front page? (work with static front page, <a href='/wp-admin/options-reading.php'>select it here</a>)", 'simplepuzzle' ),
			"id"      => "home_type",
			"std"     => "magazine",
			"type"    => "select",
			"options" => array(
				'text'     => __( "Static page text", 'simplepuzzle' ),
				'magazine' => __( "Custom posts (use below options)", 'simplepuzzle' ),
			)
		),


		array(
			"name" => __( "Show TOP 3 categories", 'simplepuzzle' ),
			"desc" => __( "Use option below to set categories name and posts count", 'simplepuzzle' ),
			"id"   => "show_topcat",
			"std"  => "1",
			"type" => "checkbox",
		),

		array(
			"name"    => __( "Category #1 last posts", 'simplepuzzle' ),
			"desc"    => "", //__("", 'simplepuzzle'),
			"id"      => "home_cat1",
			"std"     => $catlist_ids[0],
			"type"    => "select",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Number posts", 'simplepuzzle' ),
			"desc"    => __( "How many posts to display?<br><br>", 'simplepuzzle' ),
			"id"      => "home_cat1_cnt",
			"std"     => "3",
			"type"    => "text",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Category #2 last posts", 'simplepuzzle' ),
			"desc"    => "",
			"id"      => "home_cat2",
			"std"     => ( count( $catlist_ids ) > 1 ) ? $catlist_ids[1] : $catlist_ids[0],
			"type"    => "select",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Number posts", 'simplepuzzle' ),
			"desc"    => __( "How many posts to display?<br><br>", 'simplepuzzle' ),
			"id"      => "home_cat2_cnt",
			"std"     => '3',
			"type"    => "text",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Category #3 last posts", 'simplepuzzle' ),
			"desc"    => "", //__("", 'simplepuzzle'),
			"id"      => "home_cat3",
			"std"     => ( count( $catlist_ids ) > 2 ) ? $catlist_ids[2] : $catlist_ids[0],
			"type"    => "select",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Number posts", 'simplepuzzle' ),
			"desc"    => __( "How many posts to display?<br><br>", 'simplepuzzle' ),
			"id"      => "home_cat3_cnt",
			"std"     => "3",
			"type"    => "text",
			"options" => $catlist,
		),

		array(
			"name" => __( "Show category with big images", 'simplepuzzle' ),
			"desc" => __( "Use option below to set category", 'simplepuzzle' ),
			"id"   => "show_bigcat",
			"std"  => "1",
			"type" => "checkbox",
		),

		array(
			"name"    => __( "Category #4 with big featured posts", 'simplepuzzle' ),
			"desc"    => "", //__("", 'simplepuzzle'),
			"id"      => "home_cat4",
			"std"     => ( count( $catlist_ids ) > 3 ) ? $catlist_ids[3] : $catlist_ids[0],
			"type"    => "select",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Number posts", 'simplepuzzle' ),
			"desc"    => __( "How many posts to display?<br><br>", 'simplepuzzle' ),
			"id"      => "home_cat4_cnt",
			"std"     => "1",
			"type"    => "text",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Enable featured posts", 'simplepuzzle' ),
			"desc"    => __( "Do you want display top 6 featured posts (by view)", 'simplepuzzle' ),
			"id"      => "show_recent_posts",
			"std"     => "1",
			"type"    => "checkbox",
			"options" => $catlist,
		),

		array(
			"name"    => __( "Title for featured posts block", 'simplepuzzle' ),
			"desc"    => __( "Enter your text", 'simplepuzzle' ),
			"id"      => "show_recent_posts_title",
			"std"     => __( 'Recent posts', 'simplepuzzle' ),
			"type"    => "text",
			"options" => $catlist,
		),


		array(
			"name"    => __( "Small slider", 'simplepuzzle' ),
			"desc"    => __( "Display square small (300x300) slider only with big featured posts on static front page", 'simplepuzzle' ),
			"id"      => "show_smallslider",
			"std"     => "0",
			"type"    => "checkbox",
//			"options" => array(
//				'image' => __( "Image", 'simplepuzzle' ),
//				'text'  => __( "Title", 'simplepuzzle' ),
//			)
		),

		array(
			"name" => __( 'Custom carousel slides', 'simplepuzzle' ),
			"desc" => __( 'Configure your custom slider for carousel', 'simplepuzzle' ),
			"id"   => "smallslider",
			"std"  => array(
				'1' => array(
					'url'   => get_template_directory_uri() . '/img/smallslidesample.jpg',
					'title' => __( 'It is cool WorsPress theme with name SimplePuzzle!', 'simplepuzzle' ),
					'link'  => '#',
					'text'  => __( 'This theme is cool, beautiful and ready-to-use! You can have some great options and simple customize with out new theme. Enjoy it!', 'simplepuzzle' ),
				),
			),
			"type" => "repeatable",
		),


		// SLIDERS
		// .........................................................................
		array(
			"name" => "<a href='#tabslider' class='tab-title slider' title='" . __( 'Slider options', 'simplepuzzle' ) . "'><span class='dashicons dashicons-images-alt2'></span><b>" . __( 'Slider', 'simplepuzzle' ) . "</b></a>",
			"id"   => "slider",
			"type" => "section"
		),


		array(
			"name" => __( "Slider", 'simplepuzzle' ),
			"desc" => __( "Display fullwidth slider", 'simplepuzzle' ),
			"id"   => "show_bigslider",
			"std"  => "0",
			"type" => "checkbox",
		),

		array(
			"name"    => __( "Where show slider", 'simplepuzzle' ),
			"desc"    => __( "Choose where you want to display big slider (use <kbd>Ctrl</kbd> for several items)", 'simplepuzzle' ),
			"id"      => "where_show_bigslider",
			"std"     => 'all',
			"type"    => "multiple_select",
			"options" => array(
				'all'     => __( "All pages", 'simplepuzzle' ),
				'home'    => __( "Front page", 'simplepuzzle' ),
				'blog'    => __( "Blog home", 'simplepuzzle' ),
				'archive' => __( "Archives", 'simplepuzzle' ),
				'post'    => __( "Posts", 'simplepuzzle' ),
				'page'    => __( "Pages", 'simplepuzzle' ),
			)
		),


		array(
			"name"    => __( "Carousel source", 'simplepuzzle' ),
			"desc"    => __( "Custom or auto slides", 'simplepuzzle' ),
			"id"      => "bigslider_showlastposts",
			"std"     => 'custom',
			"type"    => "select",
			"options" => array(
				'custom'        => __( "Your custom items", 'simplepuzzle' ),
				'recentposts'   => __( "Recent posts", 'simplepuzzle' ),
				'featuredposts' => __( "Featured posts", 'simplepuzzle' ),
			)
		),

		array(
			"name" => __( "Posts number", 'simplepuzzle' ),
			"desc" => __( "Enter posts number for slider", 'simplepuzzle' ),
			"id"   => "slide_number",
			"std"  => '4',
			"type" => "text",
		),

		array(
			"name" => __( "Summary length", 'simplepuzzle' ),
			"desc" => __( "Enter text length for posts excerpt. When set to `0` or empty, summary will not display", 'simplepuzzle' ),
			"id"   => "slide_textlength",
			"std"  => '30',
			"type" => "text",
		),

		array(
			"name" => __( 'Custom carousel slides', 'simplepuzzle' ),
			"desc" => __( 'Configure your custom slider for carousel', 'simplepuzzle' ),
			"id"   => "custom_slider",
			"std"  => array(
				'1' => array(
					'url'   => get_template_directory_uri() . '/img/slidesample.jpg',
					'title' => __( 'It is cool WorsPress theme with name SimplePuzzle!', 'simplepuzzle' ),
					'link'  => '#',
					'text'  => __( 'This theme is cool, beautiful and ready-to-use! You can have some great options and simple customize with out new theme. Enjoy it!', 'simplepuzzle' ),
				),
			),
			"type" => "repeatable",
		),


		// POST
		// .........................................................................
		array(
			"name" => "<a href='#tabpost' class='tab-title Home page' title='" . __( 'Post options', 'simplepuzzle' ) . "'><span class='dashicons dashicons-media-default'></span><b>" . __( 'Post', 'simplepuzzle' ) . "</b></a>",
			"id"   => "post",
			"type" => "section"
		),

		array(
			"name" => __( "Show thumbnail", 'simplepuzzle' ),
			"desc" => __( "Show image thumbnail on single post before content", 'simplepuzzle' ),
			"id"   => "show_thumbnail_onsingle",
			"std"  => "1",
			"type" => "checkbox",
		),


		array(
			"name"    => __( "Meta information", 'simplepuzzle' ),
			"desc"    => __( "Select meta data to display on single posts (use <kbd>Ctrl</kbd> to multiply select).", 'simplepuzzle' ),
			"id"      => "single_meta_data",
			"std"     => "all",
			"type"    => "multiple_select",
			"options" => array(
				'all'        => __( 'Show all', 'simplepuzzle' ),
				'date'       => __( 'Published date', 'simplepuzzle' ),
				'categories' => __( 'Categories', 'simplepuzzle' ),
				'comments'   => __( 'Comments', 'simplepuzzle' ),
				'tags'       => __( 'Tags', 'simplepuzzle' ),
			)
		),

		array(
			"name" => __( "Post navigation on single", 'simplepuzzle' ),
			"desc" => __( "Hide links on previous and next posts after entry content", 'simplepuzzle' ),
			"id"   => "hide_prev_next_onsingle",
			"std"  => "0",
			"type" => "checkbox",
		),


		array(
			"name" => __( "Featured posts", 'simplepuzzle' ),
			"desc" => __( "Show recent posts after content", 'simplepuzzle' ),
			"id"   => "show_recent_posts_onsingle",
			"std"  => "1",
			"type" => "checkbox",
		),

		array(
			"name" => __( "Title before featured posts", 'simplepuzzle' ),
			"desc" => __( "Enter you text, which will be shown before recent posts block", 'simplepuzzle' ),
			"id"   => "title_recent_posts_onsingle",
			"std"  => __( 'Featured posts', 'simplepuzzle' ),
			"type" => "text",
		),

		array(
			"name" => __( "Number of featured posts", 'simplepuzzle' ),
			"desc" => __( "How many posts show", 'simplepuzzle' ),
			"id"   => "number_recent_posts_onsingle",
			"std"  => 6,
			"type" => "text",
		),

		array(
			"name"    => __( "What show in featured posts", 'simplepuzzle' ),
			"desc"    => __( "How find entries for featured posts block", 'simplepuzzle' ),
			"id"      => "source_recent_posts_onsingle",
			"std"     => "rand",
			"type"    => "select",
			"options" => array(
				'rand_all' => __( 'Random posts', 'simplepuzzle' ),
				'rand_cat' => __( 'Random posts by same category(-ies)', 'simplepuzzle' ),
				'last_cat' => __( 'Recent posts by same category(-ies)', 'simplepuzzle' ),
				// 'categorybyviews' => __( 'Most viewed from category', 'simplepuzzle'),
			)
		),


		// SOCIAL
		// .........................................................................
		array(
			"name" => "<a href='#tabsocial' class='tab-title social' title='" . __( 'Social buttons', 'simplepuzzle' ) . "'><span class='dashicons dashicons-twitter'></span><b>" . __( 'Social', 'simplepuzzle' ) . "</b></a>",
			"id"   => "social",
			"type" => "section"
		),

		array(
			"name" => __( "Twitter", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "tw",
			"std"  => '#',
			"type" => "text",
		),

		array(
			"name" => __( "Facebook", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "fb",
			"std"  => '#',
			"type" => "text",
		),

		array(
			"name" => __( "VK.com", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "vk",
			"std"  => '#',
			"type" => "text",
		),

		array(
			"name" => __( "Google+", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "gp",
			"std"  => '#',
			"type" => "text",
		),

		array(
			"name" => __( "Odnoklassniki", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "ok",
			"std"  => '#',
			"type" => "text",
		),

		array(
			"name" => __( "YouTube", 'simplepuzzle' ),
			"desc" => __( "URL to you page (link show on topbar)", 'simplepuzzle' ),
			"id"   => "yt",
			"std"  => '#',
			"type" => "text",
		),


		array(
			"name" => __( "Social data", 'simplepuzzle' ),
			"desc" => __( "Add Open Graph tags to &lt;head&gt;", 'simplepuzzle' ),
			"id"   => "add_social_meta",
			"std"  => "0",
			"type" => "checkbox",
		),


		array(
			"name"    => __( "Social share buttons", 'simplepuzzle' ),
			"desc"    => __( "Select option to show or hide social share buttons after post", 'simplepuzzle' ),
			"id"      => "social_share",
			"std"     => 'custom',
			"type"    => "select",
			"options" => array(
				'hide'    => __( "Hide", 'simplepuzzle' ),
				'custom'  => __( "Custom theme buttons", 'simplepuzzle' ),
				'share42' => __( "Share42 Buttons", 'simplepuzzle' ),
				'yandex'  => __( "Yandex Buttons", 'simplepuzzle' ),
			),
		),


		// CODE
		// .........................................................................
		array(
			"name" => "<a href='#tabcode' class='tab-title code' title='" . __( 'Additional code', 'simplepuzzle' ) . "'><span class='dashicons dashicons-editor-code'></span><b>" . __( 'Code', 'simplepuzzle' ) . "</b></a>",
			"id"   => "code",
			"type" => "section"
		),


		array(
			"name" => __( "Scripts in header", 'simplepuzzle' ),
			"desc" => __( "HTML code in &lt;head&gt; tag", 'simplepuzzle' ),
			"id"   => "head_scripts",
			"std"  => "<!-- header html from theme option -->",
			"type" => "textarea",
		),

		array(
			"name" => __( "Scripts in site footer", 'simplepuzzle' ),
			"desc" => __( "HTML code before &lt;/body&gt; tag", 'simplepuzzle' ),
			"id"   => "footer_scripts",
			"std"  => "<!-- footer html from theme option -->",
			"type" => "textarea",
		),

		array(
			"name" => __( "Custom styles", 'simplepuzzle' ),
			"desc" => __( "Add your custom CSS styles", 'simplepuzzle' ),
			"id"   => "custom_styles",
			"std"  => "",
			"type" => "textarea",
		),


		// SOCIAL
		// .........................................................................
		array(
			"name" => "<a href='#tabstructureddata' class='tab-title structured-data' title='" . __( 'Structured Data', 'simplepuzzle' ) . "'><span class='dashicons dashicons-schedule'></span><b>" . __( 'Structured Data', 'simplepuzzle' ) . "</b></a>",
			"id"   => "structureddata",
			"type" => "section"
		),

		array(
			"name" => __( "Schema.org mark up", 'simplepuzzle' ),
			"desc" => __( "Enable Schema.org mark up for posts", 'simplepuzzle' ),
			"id"   => "use_schema",
			"std"  => "1",
			"type" => "checkbox",

		),

		array(
			"name"    => __( "Schema.org type", 'simplepuzzle' ),
			"desc"    => __( "Choose Schema.org type to use. Read more in <a href='https://developers.google.com/structured-data/rich-snippets/articles'>Google recommendation</a>", 'simplepuzzle' ),
			"id"      => "schema_type",
			"std"     => "Article",
			"type"    => "radiogroup",
			"options" => array(
				'Article'     => __( "Article (<a href='http://schema.org/Article'>schema.org/Article</a>)", 'simplepuzzle' ),
				'NewsArticle' => __( "NewsArticle (<a href='http://schema.org/NewsArticle'>schema.org/NewsArticle</a>)", 'simplepuzzle' ),
				'BlogPosting' => __( "BlogPosting (<a href='http://schema.org/BlogPosting'>schema.org/BlogPosting</a>)", 'simplepuzzle' ),
			),
		),

		array(
			"name" => __( "Mark up posts list (home and archives)", 'simplepuzzle' ),
			"desc" => __( "Enable Schema.org mark up posts list (choose schema type below)", 'simplepuzzle' ),
			"id"   => "use_postslist_schema",
			"std"  => "0",
			"type" => "checkbox",

		),

		array(
			"name"    => __( "Schema.org Mark up", 'simplepuzzle' ),
			"desc"    => __( "Choose what shcema type you want use", 'simplepuzzle' ),
			"id"      => "postslist_schema_type",
			"std"     => "WebPage",
			"type"    => "radiogroup",
			"options" => array(
				'WebPage'         => __( "WebPage (<a href='http://schema.org/WebPage'>schema.org/WebPage</a>)", 'simplepuzzle' ),
				'WebSite'         => __( "WebSite (<a href='http://schema.org/WebSite'>schema.org/WebSite</a>)", 'simplepuzzle' ),
				'Blog'            => __( "Blog (<a href='http://schema.org/Blog'>schema.org/Blog</a>)", 'simplepuzzle' ),
				'LiveBlogPosting' => __( "LiveBlogPosting (<a href='http://schema.org/LiveBlogPosting'>schema.org/LiveBlogPosting</a>)", 'simplepuzzle' ),
			),
		),


		array(
			"name" => __( "Publisher logo", 'simplepuzzle' ),
			"desc" => __( "use in https://schema.org/Organization", 'simplepuzzle' ),
			"id"   => "markup_logo",
			"std"  => get_template_directory_uri() . '/img/logo.jpg',
			"type" => "text",
		),

		array(
			"name" => __( "Adress", 'simplepuzzle' ),
			"desc" => __( "use in https://schema.org/Organization", 'simplepuzzle' ),
			"id"   => "markup_adress",
			"std"  => "Russia",
			"type" => "text",
		),

		array(
			"name" => __( "Phone", 'simplepuzzle' ),
			"desc" => __( "use in https://schema.org/Organization", 'simplepuzzle' ),
			"id"   => "markup_telephone",
			"std"  => "+7 (000) 000-000-00",
			"type" => "text",
		),


	);



	$defaults = array();

	// create defaults array
		foreach ( $avd_options as $val ){
			if( is_array($val) && isset($val['std']) )
				$defaults[ $val['id'] ] = $val['std'];
		}

	// create option in database
	if ( $return_defaults ) {
		return $defaults;
	}

	add_option( SMPZ_OPTION_NAME, $defaults, '', 'yes' );

	return $avd_options;

}
// }

function simplepuzzle_get_options_list_defaults() {


}
/* ============================================================================= */



/* =============================================================================
 * load settings in admin console only
 * ============================================================================= */
//if ( is_admin() ) :




/* -----------------------------------------------------------------------------
 * register options and callback function
 * ----------------------------------------------------------------------------- */
function simplepuzzle_register_settings() {
	
	$avd_options = simplepuzzle_get_options_list();

	// options register
    // register_setting( $optionname, $optionname); //, 'simplepuzzle_validate_options' );
    register_setting( SMPZ_OPTION_NAME, SMPZ_OPTION_NAME, 'simplepuzzle_validate_options' );

	$current_section = '';
	foreach ($avd_options as $opt) {
		switch ( $opt['type'] ) {

			// new section
			case "section":
				add_settings_section( 'simplepuzzle_'.$opt['id'].'_section', $opt['name'], 'simplepuzzle_display_section', 'simplepuzzle_options_page' );
				$current_section = 'simplepuzzle_'.$opt['id'].'_section';
				break;

            // new filelds
			case "text":
			case "textarea":			
			case "checkbox":
			case "repeatable":
				$field_args = array(
					'type'      => $opt['type'],
					'id'        => $opt['id'],
					'name'      => $opt['id'],
					'desc'      => $opt['desc'],
					'std'       => $opt['std'],
					'label_for' => $opt['id'],
					'class'     => 'avd_input avd_text item-'.$opt['id'],
				);
				add_settings_field( $opt['id'].'_textbox', $opt['name'], 'simplepuzzle_display_setting', 'simplepuzzle_options_page', $current_section, $field_args );
				break;

			// selects
			case "select":
			case "check_select":
			case "multiple_select":
			case "radiogroup":
				$field_args = array(
					'type'      => $opt['type'],
					'id'        => $opt['id'],
					'name'      => $opt['id'],
					'desc'      => $opt['desc'],
					'std'       => $opt['std'],
					'label_for' => $opt['id'],
					'class'     => 'avd_input avd_select item-'.$opt['id'],
					'options_list'	=> $opt['options']
				);
				add_settings_field( $opt['id'].'_select', $opt['name'], 'simplepuzzle_display_setting', 'simplepuzzle_options_page', $current_section, $field_args );
				break;

			// colorbox
			case "color":
				$field_args = array(
					'type'      => $opt['type'],
					'id'        => $opt['id'],
					'name'      => $opt['id'],
					'desc'      => $opt['desc'],
					'std'       => $opt['std'],
					'label_for' => $opt['id'],
					'class'     => 'avd_input avd_color item-'.$opt['id'],
				);
				add_settings_field( $opt['id'].'_color', $opt['name'], 'simplepuzzle_display_setting', 'simplepuzzle_options_page', $current_section, $field_args );
				break;

			// image
			case "image":
				$field_args = array(
					'type'      => $opt['type'],
					'id'        => $opt['id'],
					'name'      => $opt['id'],
					'desc'      => $opt['desc'],
					'std'       => $opt['std'],
					'label_for' => $opt['id'],
					'class'     => 'avd_input avd_image item-'.$opt['id'],
				);
				add_settings_field( $opt['id'].'_image', $opt['name'], 'simplepuzzle_display_setting', 'simplepuzzle_options_page', $current_section, $field_args );
				break;
		}
	}
}
add_action( 'admin_init', 'simplepuzzle_register_settings' );
/* ----------------------------------------------------------------------------- */



/* -----------------------------------------------------------------------------
 * display all options
 * ----------------------------------------------------------------------------- */
function simplepuzzle_display_setting( $args ) {

    //extract( $args );

    $options = get_option( SMPZ_OPTION_NAME );

    switch ( $args['type'] ) {

		case 'text':
			wppz_print_input_option_field( $options, $args['id'], $args['std'], $args['class'], $args['desc'] );
			break;

		case 'textarea':
			wppz_print_textarea_input_field( $options, $args['id'], $args['std'], $args['class'], $args['desc'] );
			break;

        case 'select':
			wppz_print_select_options_field( $options,  $args['id'], $args['std'], $args['class'], $args['options_list'], $args['desc'] );
			break;

		case 'multiple_select':
			wppz_print_multiple_select_options_field( $options, $args['id'], $args['std'], $args['class'], $args['options_list'], $args['desc'] );
			break;

		case 'checkbox':
			wppz_print_checkbox_options_field( $options,$args['id'], $args['std'], $args['class'], $args['desc'] );
			break;

		case 'check_select':
			wppz_print_check_select_options_field( $options, $args['id'], $args['std'], $args['class'], $args['options_list'], $args['desc'] );
			break;

		case 'radiogroup':
			wppz_print_radiogroup_options_field( $options, $args['id'], $args['std'], $args['class'], $args['options_list'], $args['desc'] );
			break;

		case 'color':
			wppz_print_color_options_field( $options, $args['id'], $args['std'], $args['class'], $args['desc'] );
			break;

		case 'image':
			wppz_print_image_options_field( $options, $args['id'], $args['std'], $args['class'] );
			break;

		case 'repeatable':
			wppz_print_repeatable_options_field( $options, $args['id'], $args['std'], $args['desc'] );
			break;

    }
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $desc
 */
function wppz_print_repeatable_options_field( $options, $id, $std, $desc ) {
	$items = ( isset( $options[ $id ] ) ) ? $options[ $id ] : $std;
	echo "<div id='repeatable'>";

	if ( empty( $items ) ) {
		$items = array( '1' => array( 'title' => '', 'url' => '', 'link' => '', 'text' => '' ) );
	}

	foreach ( $items as $iter => $block ) {
		// foreach ( $options[$id] as $iter => $block ) {
		echo "<div class='repeat_block'>";
		echo "<div class='remove_block' title='" . __( 'Delete this slide', 'simplepuzzle' ) . "'></div>";
		echo "<p class='repeat_title'>" . __( 'Slide', 'simplepuzzle' ) . " $iter</p>";
		foreach ( $block as $key => $value ) {
			switch ( $key ) {
				case 'title':
					$label = __( 'Title', 'simplepuzzle' );
					echo "<div><p>$label: </p><input name='" . SMPZ_OPTION_NAME . "[$id][$iter][$key]' type='text' class='regular-text' value='$value'></div>";
					break;
				case 'url':
					$img_prev = ( $value ) ? '<img src="' . $value . '" alt="slide image">' : '';
					$label    = __( 'Image', 'simplepuzzle' );
					echo "<div><div class='img_prev'>$img_prev</div>";
					echo "<p>$label: </p><input name='" . SMPZ_OPTION_NAME . "[$id][$iter][$key]' type='text' class='text' value='$value'>";
					echo "<input type='button' class='button slider_upload_image' value='" . __( 'Upload image', 'fashionista' ) . "'/>";
					echo "</div>";
					break;
				case 'link':
					$label = __( 'Link', 'simplepuzzle' );
					echo "<div><p>$label: </p><input name='" . SMPZ_OPTION_NAME . "[$id][$iter][$key]' type='text' class='regular-text' value='$value'></div>";
					break;
				case 'text':
					$label = __( 'Description', 'simplepuzzle' );
					echo "<div class='textarea-desc'><p>$label: </p><textarea style='width:350px' rows='5' name='" . SMPZ_OPTION_NAME . "[$id][$iter][$key]'>$value";
					echo "</textarea></div>";
					break;
			}
		}

		echo "</div>";
	}
	echo "</div>";
	echo "<p><input type='button' class='button randomblock_add' value='" . __( 'Add new slide', 'simplepuzzle' ) . "' /></p>";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span>" : "";
}


/* ----------------------------------------------------------------------------- */




/* -----------------------------------------------------------------------------
 * section description
 * ----------------------------------------------------------------------------- */
function simplepuzzle_display_section( $section ){
 
 	echo '<!-- new section -->';

}
/* ----------------------------------------------------------------------------- */




/* -----------------------------------------------------------------------------
 * add options page in console
 * ----------------------------------------------------------------------------- */
function simplepuzzle_options_menu() {
	global $themename;

	$hook_suffix = add_theme_page( __("Theme options", 'simplepuzzle'), __("Theme options", 'simplepuzzle'),
		'manage_options', 'simplepuzzle_options_page', 'simplepuzzle_options_func'
	);
    add_action( "admin_print_scripts-$hook_suffix", 'simplepuzzle_admin_print_scripts' );
    
    add_action( "admin_footer-$hook_suffix", 'for_php_5_2' );
}
add_action( 'admin_menu', 'simplepuzzle_options_menu' );
/* ----------------------------------------------------------------------------- */

function for_php_5_2() {
	echo '<script>jQuery(document).ready(function() { postboxes.add_postbox_toggles(pagenow); } );</script>';
}


/* -----------------------------------------------------------------------------
 * load scripts & styles in options page
 * ----------------------------------------------------------------------------- */
function simplepuzzle_admin_print_scripts(){
	$dir = get_template_directory_uri();

    wp_enqueue_style( 'farbtastic' );
	wp_enqueue_style( 'simplepuzzle-style', SMPZ_TEMPL_URI . '/inc/admin/options/themeoptions.css' );

	wp_enqueue_media();
    wp_enqueue_script( 'postbox' );
    wp_enqueue_script( 'farbtastic' );
	wp_enqueue_script( 'simplepuzzle-script', SMPZ_TEMPL_URI . '/inc/admin/options/themeoptions.js', array('jquery', 'farbtastic', 'media-upload'));
	wp_localize_script( 
		'simplepuzzle-script', 
		'simplzObj', 
		array(
			'options_reset' => __('You are trying to reset all theme options to default values. To confirm that you want to reset all setting, enter YES in field below', 'simplepuzzle'),
		) 
	);

}
/* ----------------------------------------------------------------------------- */



/* -----------------------------------------------------------------------------
 * create option page
 * ----------------------------------------------------------------------------- */
function simplepuzzle_options_func() {
//	global $avd_options, $themename, $optionname;

	/* This checks whether the form has just been submitted */
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	$opt = get_option(SMPZ_OPTION_NAME);
?>

<div id="themeoptions" class="wrap" style="width:98%;">
	<h2><?php _e("Theme options", 'simplepuzzle') ?></h2>
	
	<?php if(isset($_REQUEST['import']) && $_REQUEST['import'] == 'import') : ?>
		<div id="message_settings_updated" class="updated">
			<p><?php _e('Options are imported successfully!','simplepuzzle') ?> </p>
		</div>
	<?php endif ?>
	<?php if(isset($_REQUEST['import']) && $_REQUEST['import'] == 'reset') : ?>
		<div id="message_settings_reseted" class="updated" >
			<p><?php _e('Options reset to the default values successfully!','simplepuzzle') ?> </p>
		</div>
	<?php endif ?>

	<?php /* If the form has just been submitted, this shows the notification */
	if ( false !== $_REQUEST['settings-updated'] ) :
		echo '<div class="updated fade"><p><strong>'. __( 'Options saved', 'simplepuzzle' ) .'</strong></p></div>';
	endif; ?>

	<form method="post" action="options.php">
	<?php	

		settings_fields( SMPZ_OPTION_NAME );
		do_settings_sections('simplepuzzle_options_page');
		submit_button(); 	

	?>
	</form>

 	<div class="import-container closed">
		<h2><?php _e('Import-Export theme settings','simplepuzzle') ?></h2>
		<form method="post" name="export" class="import_form_class export_form">
			<input type="hidden" class="action_name" name="import" value="export" />
			<input type="submit" class="button importer"  value="<?php _e('Export theme settings to file','simplepuzzle') ?>" />
		</form>
		<form method="post" name="reset" class="import_form_class reset_form">
			<input type="hidden" class="action_name" name="import" value="reset" />
			<input type="submit" class="button importer" data-value="reset" value="<?php _e('Reset theme setting to default','simplepuzzle') ?>" />
		</form>
		<form enctype="multipart/form-data" method="post" name="import_form" action="" class="import_form_class import_form">
			<input name="my_file_upload" type="file" required />
			<?php wp_nonce_field( 'my_file_upload', 'fileup_nonce' ); ?>
			<input type="hidden" class="action_name" name="import" value="import" /><br>
			<input type="submit" class="button" value="<?php _e('Import theme settings from file','simplepuzzle') ?>" />
		</form>
	</div>

</div>
<?php
}
/* ----------------------------------------------------------------------------- */




/* -----------------------------------------------------------------------------
 * validate option values
 * ----------------------------------------------------------------------------- */
function simplepuzzle_validate_options( $input ) {
	global $avd_options;

	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['tw'] = wp_filter_nohtml_kses( $input['tw'] );
	$input['fb'] = wp_filter_nohtml_kses( $input['fb'] );
	$input['gp'] = wp_filter_nohtml_kses( $input['gp'] );
	$input['vk'] = wp_filter_nohtml_kses( $input['vk'] );    
	$input['ok'] = wp_filter_nohtml_kses( $input['ok'] );    
	$input['yt'] = wp_filter_nohtml_kses( $input['yt'] );    

	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	// $input['ad_code'] = wp_filter_post_kses( $input['ad_code'] );

	$slides = $input['custom_slider'];
	if ( count($slides) ) {
		foreach ($slides as $key => $value) {
			$unset = true;
			foreach ($value as $id => $val) {
				$unset = ( $unset && empty($val) );
			}
			if ($unset) {
				unset( $input['custom_slider'][$key] );
			}
		}
	}	

	$slides_small = $input['smallslider'];
	if ( count($slides_small) ) {
		foreach ($slides_small as $i => $slide) {
			$unset = true;
			foreach ($slide as $id => $val) {
				$unset = ( $unset && empty($val) );
			}
			if ($unset) {
				unset( $input['smallslider'][$i] );
			}
		}
	}

	// checkbox unset items
	$checkbox_options = array( 
		'showsitedesc',
		'show_topcat',
		'show_bigcat',
		'show_recent_posts',
		'show_recent_posts_onsingle',
		'show_bigslider',
		'show_smallslider',
		'add_social_meta',
		'use_schema',
		'use_postslist_schema',
	);
	foreach ( $checkbox_options as $checkopt ) {
		$input[$checkopt] = isset( $input[$checkopt] ) ? $input[$checkopt] : '0';
	}

	return $input;
}

// -----------------------------------------------------------------------------


/* ============================================================================= */
//endif;


/* ============================================================================= *
 * Settigns import/export
 * ============================================================================= */

if(isset($_REQUEST['import'])) {
	require_once( SMPZ_TEMPL_PATH .'/inc/admin/importer.php' );
	add_action('init', 'wppz_export_theme_options');
}

function wppz_export_theme_options(){

	$get_defaults = true;
	$defaults = simplepuzzle_get_options_list( $get_defaults );

	$file = ( isset($_FILES) && array_key_exists( 'my_file_upload', $_FILES ) )
				? $_FILES['my_file_upload']['tmp_name'] 
				: null;
				
	new WPPZ_Importer( $_REQUEST['import'], $file, $defaults );

}
/* ============================================================================= */





/* ============================================================================= *
 * Load admin defaults
 * ============================================================================= */


if ( !class_exists('WPPuzzleAdmin') ) {
	require_once ( SMPZ_TEMPL_PATH . '/inc/admin/admin.php' );
}
$wppz_admin = new WPPuzzleAdmin( APP_NAME, false );


add_action( 'init', 'simplepuzzle_init_validate' );
function simplepuzzle_init_validate(){

    /*if ( is_admin() ) {
        return;
    }*/

    //$status = get_option( APP_NAME . '_activate' );
    //жадность и привязка к одному сайту, это фу-фу-фу
    $status = 'active';

//    if ( false == $status ) {

	if ( !class_exists('WPPuzzleAdmin') ) {
		require_once ( SMPZ_TEMPL_PATH . '/inc/admin/admin.php' );
	}

    $wppz_admin = new WPPuzzleAdmin( APP_NAME, false);

//    }



    if ( 'forbidden' == $status && !is_admin() ) {

    	header('Content-Type:text/html; charset=UTF-8');
        echo __('<h1>Error checking product license!</h1><p>To find out why, please contact us by','simplepuzzle')
         	 . ' <a href="mailto:'. antispambot('wppuzzle@gmail.com') .'">email</a>.</p>';
		die();
    }

}
/* ============================================================================= */

