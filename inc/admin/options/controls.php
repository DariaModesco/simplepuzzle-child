<?php


/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 */
function wppz_print_image_options_field( $options, $id, $std, $class ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	echo "<input class='$class regular-text code' type='text' id='$id' name='" . SMPZ_OPTION_NAME . "[$id]' value='$options[$id]' />";
	echo '<input type="button" name="upload-btn" class="button-secondary upload-btn" value="' . __( 'Upload image', 'simplepuzzle' ) . '">';
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $desc
 */
function wppz_print_color_options_field( $options, $id, $std, $class, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	echo "<div class=\"farb-popup-wrapper\">";
	echo "<input class='$class popup-colorpicker' type='text' id='$id' name='" . SMPZ_OPTION_NAME . "[$id]' value='$options[$id]' />";
	echo "<div id='" . $id . "picker' class='color-picker'></div>";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span></div>" : "</div>";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $options_list
 * @param $class
 * @param $desc
 */
function wppz_print_radiogroup_options_field( $options, $id, $std, $class, $options_list, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	echo "<div id='" . $id . "' class='radio_group'>";
	$cnt = 1;
	// only for custom option
	$disabled      = array();
	$is_postschema = ( 'postslist_schema_type' == $id );
	if ( $is_postschema ) {
		switch ( $options['schema_type'] ) {
			case 'Article':
			case 'NewsArticle':
				$disabled = array( 'WebPage', "WebSite" );
				break;
			case 'BlogPosting':
			default:
				$disabled = array( 'Blog', "LiveBlogPosting" );
				break;
		}
	}
	foreach ( $options_list as $val => $title ) {
		$checked = ( $val == $options[ $id ] ) ? "checked='checked'" : "";
		$st      = ( ! in_array( $val, $disabled ) && $is_postschema ) ? ' style="opacity:0.4" ' : '';
		$dis     = ( ! in_array( $val, $disabled ) && $is_postschema ) ? ' disabled="disabled" ' : '';
		echo "<label $st for='" . SMPZ_OPTION_NAME . "[$id]-$cnt' ><input type='radio' name='" . SMPZ_OPTION_NAME . "[$id]' value='$val' id='" . SMPZ_OPTION_NAME . "[$id]-$cnt' $checked $dis class='$class' />$title</label>";
		$cnt ++;
	}
	echo "</div>";
	echo ( $desc != '' ) ? "<span class='description'>$desc</span>" : "";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $options_list
 * @param $class
 * @param $desc
 */
function wppz_print_check_select_options_field( $options, $id, $std, $class, $options_list, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? $options[ $id ] : $std;
	echo "<div class='checkbox_group'>";
	$cnt = 1;
	foreach ( $options_list as $val => $title ) {
		$checked = ( in_array( $val, $options[ $id ] ) ) ? "checked='checked'" : "";
		echo "<label for='" . SMPZ_OPTION_NAME . "[$id]-$cnt' ><input type='checkbox' name='" . SMPZ_OPTION_NAME . "[$id][]' value='$val' id='" . SMPZ_OPTION_NAME . "[$id]-$cnt' $checked class='$class' />$title</label>";
		$cnt ++;
	}
	echo "</div>";
	echo ( $desc != '' ) ? "<span class='description'>$desc</span>" : "";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $desc
 */
function wppz_print_checkbox_options_field( $options, $id, $std, $class, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	$checked        = ( $options[ $id ] ) ? "checked='checked'" : "";
	echo "<label for='$id'><input type='checkbox' name='" . SMPZ_OPTION_NAME . "[$id]' id='$id' value='true' $checked class='$class' /> $desc</label>";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $options_list
 * @param $desc
 */
function wppz_print_multiple_select_options_field( $options, $id, $std, $class, $options_list, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? $options[ $id ] : $std;
	echo "<select multiple name='" . SMPZ_OPTION_NAME . "[$id][]' class='$class' id='$id' />";
	foreach ( $options_list as $val => $title ) {
		$sel = ( $val == $options[ $id ] || in_array( $val, $options[ $id ] ) ) ? " selected='selected'" : "";
		echo "<option$sel value=\"$val\">$title</option>";
	}
	echo "</select>";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span>" : "";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $options_list
 * @param $desc
 */
function wppz_print_select_options_field( $options, $id, $std, $class, $options_list, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	echo "<select name='" . SMPZ_OPTION_NAME . "[$id]' class='$class' id='$id' />";
	foreach ( $options_list as $val => $title ) {
		$sel = ( $val == $options[ $id ] ) ? " selected='selected'" : "";
		echo "<option$sel value=\"$val\">$title</option>";
	}
	echo "</select>";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span>" : "";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $desc
 */
function wppz_print_textarea_input_field( $options, $id, $std, $class, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) )
		? esc_attr( stripslashes( $options[ $id ] ) )
		: $std;
	echo "<textarea class='regular-text $class' type='textarea' id='$id' name='" . SMPZ_OPTION_NAME . "[$id]' cols='40' rows='5' >" . $options[ $id ] . "</textarea>";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span>" : "";
}

/**
 * @param $options
 * @param $id
 * @param $std
 * @param $class
 * @param $desc
 */
function wppz_print_input_option_field( $options, $id, $std, $class, $desc ) {
	$options[ $id ] = ( isset( $options[ $id ] ) ) ? esc_attr( stripslashes( $options[ $id ] ) ) : $std;
	echo "<input class='regular-text $class' type='text' id='$id' name='" . SMPZ_OPTION_NAME . "[$id]' value='$options[$id]' />";
	echo ( $desc != '' ) ? "<br /><span class='description'>$desc</span>" : "";
}