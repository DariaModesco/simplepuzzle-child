jQuery(document).ready(function($) {

	$('#themeoptions .form-table').each( function(){
		var name = $(this).prev().find('a').attr('href');
		$(this).wrap('<div id="' + name.substr(1) + '" class="tabitem"></div>');
	});
	
	$('#themeoptions form > h3:not(:first) a').appendTo( $('#themeoptions form > h3:first').attr('id','tabs') );
	$('#themeoptions form > h3:not(:first)').remove();
	
	$('#themeoptions form > h2:not(:first) a').appendTo( $('#themeoptions form > h2:first').attr('id','tabs') );
	$('#themeoptions form > h2:not(:first)').remove();

	$(".tabitem").hide();
	$("#tabs a:first").addClass('active');
	$(".tabitem:first").show();
	$("#tabs a").click(function() {
		$("#tabs a").removeClass("active");
		$(this).addClass("active");
		$(".tabitem").hide();
		var actTab = $(this).attr("href");
		$(actTab).fadeIn(300);
		return false;
	});
	
	var $color_inputs = $('input.popup-colorpicker');
	$color_inputs.each(function(){
		var $input = $(this);
		var $pickerId = "#" + $(this).attr('id') + "picker";

		$($pickerId).hide();
		$($pickerId).farbtastic($input);
		$($input).focus(function(){ $($pickerId).slideDown() });
		$($input).blur(function(){ $($pickerId).slideUp() });
	});

    // media library
    $('.upload-btn').click(function(e) {
        e.preventDefault();
        var img_url = $(this).prev(),
            image = wp.media({ 
                title: 'Загрузить',
                multiple: false,
                button: { text: 'Выбрать' },
            }).open()
            .on('select', function(e){
                var uploaded_image = image.state().get('selection').first();
                // var image_thumb_url = uploaded_image.toJSON().sizes.thumbnail.url;
                var image_url = uploaded_image.toJSON().url;
                $(img_url).val( image_url );
                // $(img_url).siblings('.image_preview').html('<img src="' + image_thumb_url + '" height="150" width="150" />');
            });
    });

    // options
    $('#schema_type input').change(function(event) {
    	var item = $(this).val(),
    		topschema = $('#postslist_schema_type');

    	$( topschema ).find("input[type='radio']")
    		.attr('disabled',false)
    		.attr( 'checked', false )
    		.parent().css('opacity', '1');

    	if ( 'Article'==item || 'NewsArticle'==item ) {

 			$( topschema ).find("input[value*='Blog']").attr('disabled', true )
 				.parent().css( 'opacity', '0.4' );
 			$( topschema ).find("input[type='radio']:enabled").eq(0).attr('checked', true );

    	} else if ( 'BlogPosting'==item ){

 			$( topschema ).find("input[value*='Web']").attr('disabled', true )
 				.parent().css( 'opacity', '0.4' );
 			$( topschema ).find("input[type='radio']:enabled").eq(0).attr('checked', true );

    	}

    });

    /* import/export    
     * _______________________________________ */
    $('form.reset_form').submit(function(event) {
    	var str = ( 'undefined'===simplzObj.options_reset) 
    				? 'Enter YES to reset all options to defaults:' 
    				: simplzObj.options_reset,
    		test = prompt( str, '');
    	if ( 'YES'==test || 'yes'==test )  return true;
    	else return false;
    });
    $('.import-container h2').click(function(event) {
    	var cont = $(this).parent();
    	$(cont).toggleClass('closed');
    });


// ----------------------------------------------------------------
// SLIDER
// ----------------------------------------------------------------
	
	/* slider options toggle 
	 * _________________________________ */
	function toggle_carousel_source( source ){
		var custom_option = $('tr.item-custom_slider'),
			other_option = $('tr.item-slide_number,tr.item-slide_textlength');
		switch ( source ){
			case 'recentposts':
			case 'featuredposts':
				$(custom_option).hide();
				$(other_option).show();
				break;
			default:
				$(custom_option).show();
				$(other_option).hide();
				break;
		}
	}
	toggle_carousel_source( $('#bigslider_showlastposts').val() );
	$('#bigslider_showlastposts').change(function(event) {
		toggle_carousel_source( $(this).val() );
	});


	/* slides render 
	 * _________________________________ */
	function replaceNumbers( blocks ){
		var counter = 1;
		// var arr = $('#repeatable .repeat_block');
		$.each( blocks, function( index, value){
			var p_title = $(value).find('p').first();
			$(p_title).text( $(p_title).text().replace( /\d/, counter) );
			$(value).find('input[type=text], textarea').each( function(){
				var n = $(this).attr('name');			
            	$(this).attr( 'name', n.replace( /\d/, counter) );
			})
			counter++;
		})
		
	}

	$('.randomblock_add').click(function(){
        var block = $(this).parent().siblings(),
            new_el = $(block).children('.repeat_block').first().clone( true );
        $(new_el).find('input[type=text], textarea').each( function(){
          	$(this).val('');
        });     
        $(new_el).find('.img_prev').html('');
        $(new_el).find('.remove_block').on( 'click', remove_function );
        $(new_el).insertAfter( $(this).parent().prev().children().last() );
        // $(new_el).insertAfter( $('#repeatable .repeat_block').last() );
		replaceNumbers( $(this).parent().prev().children() );
    });
	
		
	$('.remove_block').bind( 'click', remove_function);
	function remove_function(){
		var this_block = $(this).closest('.repeat_block'),
			blocks = $(this_block).siblings();

		if( blocks.length >= 1){
			$(this).parent('.repeat_block').remove();
			replaceNumbers( blocks );
		}
		else {
			// console.log('--------');
			// $('#repeatable .repeat_block').find('input[type=text]').each( function(){
			$(this_block).find('input[type=text], textarea').each( function(){
				$(this).val('');
			});
			$(this_block).find('.img_prev').html('');
		}
	}


    $('.slider_upload_image').click(function(e) {
        e.preventDefault();
        var img_url = $(this).prev(),
        	findsmallslider = $(this).closest('.item-smallslider').length,
            image = wp.media({ title: 'Загрузить', multiple: false, button: { text: 'Выбрать' }, })
	            .open()
	            .on('select', function(e){
	                var uploaded_image = image.state().get('selection').first(),
	                    image_url = uploaded_image.toJSON().sizes,
	                    ok = false;
	                if ( findsmallslider > 0 && "medium" in image_url ) {
	                	image_url = image_url.medium.url;
	                	ok = true;
	                } 
	                if ( !findsmallslider && "large" in image_url ) {
	                	image_url = image_url.large.url;
	                	ok = true;
	                }
	                console.log( image_url );
	                if ( !ok )  {
	                	image_url = image_url.full.url;
	                }
	                $(img_url).val( image_url );
	                $(img_url).siblings('.img_prev').html( '<img src="' + image_url + '" alt="slide">' );
	            });
    });		
// ----------------------------------------------------------------

	
});
