<?php


require_once ABSPATH.WPINC.'/class-wp-customize-control.php';



/* ========================================================================
 *            FONT control
 * ======================================================================== */

class WP_Customize_Font_Control extends WP_Customize_Control
{


    /**
     * @var string
     */
    public $content = '';

    /**
     * @return null
     */
    public function render_content()
    {

        if (empty($this->choices)) {
            return;
        }

        ?>
        <label>
            <?php if ( ! empty($this->label)): ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif;
        if ( ! empty($this->description)): ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif;?>

            <select <?php $this->link();?>>
                <?php
                $iter = 0;
                echo '<option value="" disabled style="font-size:16px;background: #535353;color:#fff;">'. __('Web safe fonts', 'simplepuzzle') .'</option>';
                foreach ($this->choices as $value => $item) {
                    $iter++;
                    $label = str_replace('_', ' ', $value);
                    $style = ' style="font-family:'.$item['css'].';font-size:1.5em;line-height:160%"';
                    echo '<option value="'.esc_attr($value).'"'.selected($this->value(), $value, false).$style.'>'.$label.'</option>';
                    if ( 6 == $iter ) {
                        echo '<option value="" disabled style="font-size:16px;background: #535353;color:#fff;">'. __('Google Fonts', 'simplepuzzle') .'</option>';
                    }
		        }
		        ?>
            </select>
        </label>
        <?php

    }
}


/* ======================================================================== */