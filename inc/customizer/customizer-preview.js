jQuery(document).ready(function($) {

	var style = $( '#simplepuzzle-customizer-css' );

	if ( !$(style).length ) {
		$(style) = $('head').append( '<style type="text/css" id="simplepuzzle-customizer-css">' )
		                    .find( '#simplepuzzle-customizer-css' );
	}





/*----------  I N F O    B A R  ----------*/
// отображение/скрытие инфо сайтбара
	if( !wp.customize.instance('true_infobar').get() ){
		$('.infobar').hide();
	}	
	wp.customize( 'true_infobar', function( value ) {
		value.bind( function( to ) {
			false === to ? $( '.infobar' ).hide() : $( '.infobar' ).show();
		} );
	});

// social buttons
	wp.customize( 'infobar_social', function( value ) {
		value.bind( function( to ) {
			var ibsoc = $('.infobar .top-social');
			$(ibsoc).removeClass('fl fr hide');
			$(ibsoc).addClass( to );
		});
	});

// infobar message
	wp.customize( 'infobar_search', function( value ) {
		value.bind( function( to ) {
			var ibsearch = $('.infobar #topsearch');
			$(ibsearch).removeClass('fl fr hide');
			$(ibsearch).addClass( to );
		});
	});

// infobar message
	wp.customize( 'infobar_message', function( value ) {
		value.bind( function( to ) {
			var ibmess = $('.infobar .top-message');
			$(ibmess).removeClass('fl fr hide');
			$(ibmess).addClass( to );
		});
	});

// infobar message
	wp.customize( 'infobar_message_text', function( value ) {
		value.bind( function( to ) {
			$('.infobar .top-message').html(to);
		});
	});


// фон инфобара
	wp.customize( 'bgcolor_infobar', function( value ) {
		value.bind( function( to ) {
		// value.bind( function( to ) {
			reg_font('.infobar,.mobmenu,.mm-button.opened{background:', to);
			// reg_font('.infobar,.mobmenu{background:', to);
		});
	});

// фон текста
	wp.customize( 'color_infobar', function( value ) {
		value.bind( function( to ) {
			reg_font('.infobar,.mobbar,#topsearch input.s,.infobar a,.top-message{color:', to);
			reg_font('.infobar .search-button,.soc-item,.infobar .search-underform{fill-opacity:1;fill:', to);
		});
	});



/*----------  H E A D E R  ----------*/

// site title
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$('#logo').attr( 'title', to);
			$('.sitelogo-txt').html(to);
			$('.sitelogo-img').attr('alt', to);
		});
	});

// site descripton
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			if ( !$('.sitedescription').length ) {
				$('.site-logo').append('<p class="sitedescription"></p>');
			}
			$('.sitedescription').html(to);
		});
	});

// hide/show descripton
	if( !wp.customize.instance(opt_name + '[showsitedesc]').get() ){
		$(opt_name + '[showsitedesc]').hide();
	}	
	wp.customize( opt_name + '[showsitedesc]', function( value ) {
		value.bind( function( to ) { 
			false === to 
				? $( '.sitedescription' ).hide() 
				: $( '.sitedescription' ).show();
		});
	});

// текст или логотип
	wp.customize( opt_name + '[logotype]', function( value ) {
		value.bind( function( to ) {
			if ( 'text' === to ) {
				if ($('.sitelogo-txt').length == 0) { //!$('#logo').length 
					$('.logo, .small-logo').append( '<span class="sitelogo-txt">'+ $('#logo').attr('alt') +'</span>');
				}
				$('.sitelogo-txt').show();
				$('.sitelogo-img').hide();
			} 
			else if('image' === to){
				if ($('.sitelogo-img').length == 0) { //
					$('.logo, .small-logo').append( '<img class="sitelogo-img" src="' 
						+ wp.customize.instance( opt_name+'[logo_image]').get() + '">');
				}
				$('.sitelogo-img').show();
				$('.sitelogo-txt').hide();
			}
		});
	});

// логотип
	wp.customize( opt_name + '[logo_image]', function( value ) {
		value.bind( function( to ) {
			if ( !$('.logo .sitelogo-img').length ) {
				$('.logo').append( '<img id="logo" class="sitelogo-img" src="'+ to + '" alt="' + $('#logo').attr('title') + '">');
			} else {
				$('.logo .sitelogo-img').attr( 'src', to);
			} 
			if ( 'text' == wp.customize.instance( opt_name + '[logotype]' ).get() ) {
				$('.logo .sitelogo-img').hide();
			}
		});
	});


// отображение/скрытие меню
	if( !wp.customize.instance('show_pages_menu').get() ){
		$('.pages-menu').hide();
	}
	wp.customize( 'show_pages_menu', function( value ) {
		value.bind( function( to ) {
			if ( false === to ) {
				$('.pages-menu').hide();
				$('.site-logo').removeClass('col4');
			} else {
				$('.pages-menu').show();
				$('.site-logo').addClass('col4');
			}
		} );
	});

	wp.customize( 'font_logo', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('.sitelogo-txt{font-family:', font);
			
		});

	});


// цвет текстового логотипа
	wp.customize( 'logo_text_color', function( value ) {
		value.bind( function( to ) {
			reg_font('.logo{color:', to);
		});
	});

// картинка для шапки
	wp.customize( 'bgimg_header', function( value ) {
		value.bind( function( to ) {
			console.log( to );
			console.log( wp.customize.instance('header_image').get() );
			//false === to ? $('.pages-menu').hide() : $('.pages-menu').show();
            
            $('#header').css( 'background-image', 'url("'+  wp.customize.instance('header_image').get() +'")' );

			switch ( to ) {
	            case 'repeat': 
			        reg_font( '#header{background-repeat:', to );
	                break;
	            case 'repeat-x': 
	                reg_font( '#header{background-repeat:', to );
	                break;
	            case 'center': 
	                reg_font( '#header{background-repeat:' , 'no-repeat' );
	                reg_font( '#header{background-position:' , 'top center' );
	                break;
	            case 'image': 
	            	if ( !$('#header .header-img').length ){
	            		$('#header .inner').append('<img src="' + wp.customize.instance('header_image').get() + '" alt="header img" class="header-img">')
	            	} else {
	            		
	            	$('#header .header-img').prop('src', wp.customize.instance('header_image').get() );
	            	$('#header .header-img').show();
	            	}
            		$('#header').css( 'background-image', 'none' );
	            	break;
			}
            if ( 'image' != to )  {
	            $('#header .header-img').hide();
			}

		} );
	});


	

/*----------  C O L O R S  ----------*/
	
// фон сайта
	wp.customize( 'true_background_image', function( value ) {
		value.bind( function( to ) {
			0 === $.trim( to ).length ? $( 'body' ).css( 'background-image', '' ) : $( 'body' ).css( 'background-image', 'url( ' + to + ')' );
		});
	});	


// фон шапки
	wp.customize( 'bgcolor_header', function( value ) {
		value.bind( function( to ) {
			reg_font('#header{background-color:', to);
		});
	});

// цвет ссылок сайта
	wp.customize( 'true_link_color', function( value ) {
		value.bind( function( to ) {
			reg_font('.entry a{color:', to);
		});
	});

// цвет ссылок при наведении
	wp.customize( 'true_link_color_hover', function( value ) {
		value.bind( function( to ) {
			reg_font('.entry a:hover{color:', to);
			reg_font('.entry a::after{background-color:', to);
		});
	});


// цвет кнопок
	wp.customize( 'button', function( value ) {
		value.bind( function( to ) {
			reg_font("a,h2,.social-share .like,.view-box a:hover,.anoncethumb:hover,.comment-content a,.left-wrap-two .small-post-bigimg .anoncethumb,.widget a:hover,#comments .widget-title,.logo,blockquote{color:", to);
			reg_font("button,input[type=button],input[type=reset],input[type=submit],.read-more,.more-link,.insider #submit-us,.wp-pagenavi .current,.post-nav-links .view:hover,.comment-form #submit,.bx-viewport,.discuss-title,#wlist #subsubmit,ol li:before,#footerbar .widget-title,.pagewrapper .searchform input.s, ul > li:before, ul > li:after,.main-menu a:hover, .main-menu .current-menu-item span, .main-menu .current-menu-item a,.page-numbers:hover,.infobar,.mobmenu,.mm-button.opened,.top-pages .sub-menu,.top-pages .children{background-color:", to);
			reg_font(".page-numbers:hover,input[type=text]:focus,input[type=password]:focus,input[type=email]:focus,input[type=url]:focus,input[type=tel]:focus,input[type=date]:focus,input[type=datetime]:focus,input[type=datetime-local]:focus,input[type=time]:focus,input[type=month]:focus,input[type=week]:focus,input[type=number]:focus,input[type=search]:focus,textarea:focus{border-color:", to);
			reg_font(".top-pages .sub-menu:before,.top-pages .children:before{border-bottom-color:", to);
			// var mrgba = hex_to_rgba(to, '0.4');
			// console.log( to + ' is ' +  mrgba );
			reg_font("blockquote{background-color:", hex_to_rgba(to, '0.4') );
			reg_font(".anoncethumb:hover .wrap-img:before{background-color:", hex_to_rgba(to, '0.85') );

		});
	});

// цвет кнопок при наведении
	wp.customize( 'button_hover', function( value ) {
		value.bind( function( to ) {
			reg_font('a:hover,.entry a:hover,.comment a:hover,.left-wrap-two .small-post-bigimg .anoncethumb:hover,.bypostauthor .comment-author .fn,.bypostauthor .comment-author a,.entry-meta a:hover{color:', to);
			reg_font('.entry a::after,button:hover,input[type=button]:hover,input[type=reset]:hover,input[type=submit]:hover,.read-more:hover,.more-link:hover,.comment-form #submit:hover{background-color:', to);
			// reg_font('button:hover,input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover,.read-more:hover,.more-link:hover{background-color:', to);
		});
	});


	
// цвет заглавий в анонсe
	wp.customize( 'true_color_head_anonce', function( value ) {
		value.bind( function( to ) {
			reg_font('.review_title a,.anoncethumb{color:', to);
		});
	});

// цвет заглавий в виджите
	wp.customize( 'color_widget', function( value ) {
		value.bind( function( to ) {
			// reg_font('.widget-title{color:', to);
			reg_font('#footerbar .widget-title{background-color:', to);
		});
	});

// цвет h1
	wp.customize( 'color_h1', function( value ) {
		value.bind( function( to ) {
			reg_font('h1{color:', to);
		});
	});

// цвет h2
	wp.customize( 'color_h2', function( value ) {
		value.bind( function( to ) {
			reg_font('h2{color:', to);
		});
	});

// цвет h3
	wp.customize( 'color_h3', function( value ) {
		value.bind( function( to ) {
			reg_font('h3{color:', to);
		});
	});

// цвет h4
	wp.customize( 'color_h4', function( value ) {
		value.bind( function( to ) {
			reg_font('h4{color:', to);	
		});
	});

// цвет h5-h6
	wp.customize( 'color_h5', function( value ) {
		value.bind( function( to ) {
			reg_font('h5, h6{color:', to);});
	});

/*----------  F O O T E R  ----------*/

// копирайт в футере
	wp.customize( 'true_footer_copyright_text', function( value ) {
		value.bind( function( to ) {
			$( '#footer_custom' ).text( to );
		});
	});	

// ссылки в футере
	wp.customize( opt_name+'[hide_wppuzzle_links]', function( value ) {
		value.bind( function( to ) {
			'hide' === to 
				? $( '#copyrights' ).hide() 
				: $( '#copyrights' ).show();
		});
	});


/*----------  F O N T S  ----------*/

	// изменение основного шрифта на сайте
	var sFont;
	wp.customize( 'body_font', function( value ) {
		value.bind( function( to ) {
			if ( 'undefined' != fontsObj ){
				to = fontsObj[ to ];
			}
			reg_font('body{font-family:', to);
		});

	});


	wp.customize( 'font_h1', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('h1{font-family:', font);
		});

	});

	wp.customize( 'font_h2', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('h2{font-family:', font);
		});

	});

	wp.customize( 'font_h3', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('h3{font-family:', font);
		});

	});

	wp.customize( 'font_h4', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('h4{font-family:', font);
		});

	});

	wp.customize( 'font_h5', function( value ) {
		value.bind( function( to ) {
			var font = to;
			if ( 'undefined' != fontsObj ){
				font = fontsObj[ to ];
			}
			reg_font('h5, h6{font-family:', font);
		});

	});

//регулярка
	function reg_font(reg, value_to){
		var style_now = $(style).text();
		var i = style_now.search(reg);

		if (i == -1)
			$(style).append( reg + value_to + '}' );
		else{
			var re = new RegExp(reg+'(.*?)\}');
			$(style).text( style_now.replace(re, reg + value_to + '}'));

		}
	}

// #ffff to rgba(0,0,0,1)
	function hex_to_rgba( hex, alpha ){
		var r, g, b, myrgba;

		hex = hex.substring(1,7);
		r = parseInt(hex.substring(0,2), 16);
		g = parseInt(hex.substring(2,4), 16);
		b = parseInt(hex.substring(4,6), 16);

		myrgba = 'rgba('+ r +','+ g +','+ b +','+ alpha +')';

		return myrgba;
	}

});