<?php get_header(); ?>
	<main id="content"<?php the_list_markup('itemtype'); ?>>
	
	<?php the_list_markup('siteinfo'); ?>

<?php if (have_posts()) : 

	the_archives_header();

	while (have_posts()) : the_post(); 
		get_template_part( 'content', 'short' ); 
	endwhile; ?>

	<?php avd_the_pagination(false); 
	
else: ?>

	<div class="post clearfix">		
	    <h2><?php _e( 'Posts not found', 'simplepuzzle' ); ?></h2>
	    <?php get_search_form(); ?>
	</div>
		
<?php endif; ?>
    

	</main> 
	<!-- END #content -->

	<?php the_list_markup('publisher'); ?>
	
<?php get_sidebar(); ?>
<?php get_footer(); ?>