/*! Picturefill - Responsive Images that work today. 
 * (and mimic the proposed Picture element with span elements). 
 * Author: Scott Jehl, Filament Group, 2012 | License: MIT/GPLv2 */
!function(a){"use strict";a.picturefill=function(){for(var b=a.document.getElementsByTagName("span"),c=0,d=b.length;d>c;c++)if(null!==b[c].getAttribute("data-picture")){for(var e=b[c].getElementsByTagName("span"),f=[],g=0,h=e.length;h>g;g++){var i=e[g].getAttribute("data-media");(!i||a.matchMedia&&a.matchMedia(i).matches)&&f.push(e[g])}var j=b[c].getElementsByTagName("img")[0];if(f.length){var k=f.pop();j&&"NOSCRIPT"!==j.parentNode.nodeName||(j=a.document.createElement("img"),j.alt=b[c].getAttribute("data-alt")),j.src=k.getAttribute("data-src"),k.appendChild(j)}else j&&j.parentNode.removeChild(j)}},a.addEventListener?(a.addEventListener("resize",a.picturefill,!1),a.addEventListener("DOMContentLoaded",function(){a.picturefill(),a.removeEventListener("load",a.picturefill,!1)},!1),a.addEventListener("load",a.picturefill,!1)):a.attachEvent&&a.attachEvent("onload",a.picturefill)}(this);

/* SimplePuzzle FUNCTIONS */
jQuery(document).ready(function($) {
	
// toTop button 
	$(window).scroll( function() {
		if( $(this).scrollTop() != 0)	$('#toTop').fadeIn();
		else 	$('#toTop').fadeOut(); 
	});
	$('#toTop').click( function() {
		$('body,html').animate( {scrollTop:0}, 500);
	});

// responsive menu 
    $('.hamburger').click( function(){
    	$('.hamburger').toggleClass('opened');
        if ( $(this).hasClass('opened') ) {
            $('#left_mobile').show();
        } else {
            $('#left_mobile').hide();
            $('.hamburger').removeClass('opened');
        }
        return false;
    });

// Sliders
if ($('.bigslider').length ) {	
	$('.bigslider').bxSlider({
		mode: 'fade',
		// slideSelector: 'slide-item',
		adaptiveHeight: true,
	});	
}
if ($('.second-slider').length ) {	
	$('.second-slider').bxSlider({
		mode: 'fade',
		slideSelector: 'li.small-slider-item',
		adaptiveHeight: true,
	});
}

//Плавная прокрутка по якорям
	$("a[href*=#comments]").bind("click", function(e){
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top
		}, 500);
		e.preventDefault();
		return false;
	});
	
// social buttons
	$('.social_item').click(function(){
	 	window.open($(this).attr("href"),'displayWindow', 'width=700,height=400,left=200,top=100,location=no, directories=no,status=no,toolbar=no,menubar=no');
	  	return false;
	});

});
