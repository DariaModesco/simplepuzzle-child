<?php get_header(); ?>
	<main id="content">

		<?php while ( have_posts() ) : the_post(); 

			get_template_part( 'content', 'full' ); 

		endwhile; 

		if ( comments_open() || get_comments_number() ) { 
			comments_template(); 
		} ?>
		
	</main> <!-- #content -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>