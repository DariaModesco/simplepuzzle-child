<?php 

if ( !in_array(simplepuzzle_get_layout(), array('full','center')) ) : ?>

	<?php if ( 'both' == simplepuzzle_get_layout() ) { ?>
	<aside id="secondbar">
		<ul class="wlist clearfix">

			<?php if ( is_active_sidebar( 'secondbar' ) ) { 
				dynamic_sidebar( 'secondbar' ); 
			} else { 
				wp_list_categories('title_li=<p class="widget-title">'. __("Categories", 'simplepuzzle') .'</p>'); 
			} ?>
		
		</ul>
	</aside>
	<?php } ?>

<?php endif; ?>