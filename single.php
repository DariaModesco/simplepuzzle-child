<?php get_header(); ?>
	<main id="content">


<?php 
	
	while (have_posts()) : the_post();  

		get_template_part( 'content', 'full' ); ?>


		<?php
		


		// recent posts
		

		// comments
		if ( comments_open() || get_comments_number() ) { 
			comments_template();
		}

	endwhile; ?>

<div id="before_footer">
  <!-- Виджет перед футером BEGIN -->
  <?php if ( function_exists('dynamic_sidebar') ) dynamic_sidebar('before_footer'); ?>
    <!-- Виджет перед футером END -->
</div>
	</main> <!-- #content -->
	<?php get_sidebar(); ?>
<?php get_footer(); ?>