<?php
class categoriesWidget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'categories_widget',
            __('Categories','My'),
            array( 'description' => __('Category menu','My') )
        );
    }
    function subcats($id) {
        $subcats = get_categories(array('parent'=>$id));
        if (count($subcats)) {
            echo '<span class="plus"></span><ul class="child">';
            foreach ($subcats as $subcat) {
                $cat_meta = get_option( "category_".$subcat->term_id);
                $link = get_category_link($subcat->term_id);
                $is_no_link = str_replace('https://','',str_replace('http://','',$link)) == str_replace('https://','',str_replace('http://','',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));
                if ( $is_no_link ) {
                    echo '<li class="current"><span class="link">';
                } else {
                    echo '<li><a class="link" href="'.$link.'">';
                }
                echo '<span class="icon"';
                if (isset($cat_meta['icon_path'])) {
                    echo ' style="background-image:url('.$cat_meta['icon_path'].')"';
                }
                echo '></span>'.$subcat->name;
                if ( $is_no_link ) {
                    echo '</span>';
                } else {
                    echo '</a>';
                }
                $this->subcats($subcat->term_id);
                echo '</li>';
            }
            echo '</ul>';
        }
    }

    public function widget( $args, $instance ) {
        echo '<div class="widget categories">';
        if ( ! empty( $instance[ 'title' ] ) ) echo '<div class="title">'.$instance[ 'title' ].'</div>';
        echo '<ul>';
        $categories = get_categories(array('parent'=>0));
        foreach ($categories as $category) {
            $cat_meta = get_option( "category_".$category->term_id);
            $link = get_category_link($category->term_id);
            $is_no_link = str_replace('https://','',str_replace('http://','',$link)) == str_replace('https://','',str_replace('http://','',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']));
            if ( $is_no_link ) {
                echo '<li class="current"><span class="link">';
            } else {
                echo '<li><a class="link" href="'.$link.'">';
            }
                echo '<span class="icon"';
            if (isset($cat_meta['icon_path'])) {
                echo ' style="background-image:url('.$cat_meta['icon_path'].')"';
            }
            echo '></span>'.$category->name;
            if ( $is_no_link ) {
                echo '</span>';
            } else {
                echo '</a>';
            }
            $this->subcats($category->term_id);
            echo '</li>';
        }
        echo '</ul></div>';
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = '';
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Header','My'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}

function categories_widget_load() {
    register_widget( 'categoriesWidget' );
}
add_action( 'widgets_init', 'categories_widget_load' );