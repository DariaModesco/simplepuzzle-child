<?php
class lastCommentsWidget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'last_comments_widget',
            __('Last comments','My'),
            array( 'description' => __('List of post, which have a last comments','My') )
        );
    }

    public function widget( $args, $instance ) {
        echo '<div class="widget last_comments">';
        if ( ! empty( $instance[ 'title' ] ) ) echo '<div class="title">'.$instance[ 'title' ].'</div>';
        if ( empty( $instance[ 'count' ] ) ) $instance[ 'count' ] = 5;
        echo '<ul>';
        $comments = get_comments( array(
                                        'number' => $instance[ 'count' ],
                                        'orderby' => 'comment_date',
                                        'order' => 'DESC',
                                        'type' => ''
                                    ) );
        foreach($comments as $comment){
            $ID = $comment->comment_post_ID;
            echo '<li><span class="name">'.$comment->comment_author.'</span> &mdash; <a class="post" href="'.post_permalink($ID).'">'.get_the_title($ID).'</a><span class="count">'.get_comments_number($ID).'</span></li>';
        }
        echo '</ul></div>';
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = '';
        }
        if ( isset( $instance[ 'count' ] ) ) {
            $count = $instance[ 'count' ];
        } else {
            $count = 5;
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Header','My'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e('Count','My'); ?></label>
            <input id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" value="<?php echo esc_attr( $count ); ?>" type="number" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
        return $instance;
    }
}

function last_comments_widget_load() {
    register_widget( 'lastCommentsWidget' );
}
add_action( 'widgets_init', 'last_comments_widget_load' );