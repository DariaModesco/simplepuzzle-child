<?php
class popularPostsWidget extends WP_Widget {

    function __construct() {
        parent::__construct(
            'popular_posts_widget',
            __('Popular posts','My'),
            array( 'description' => __('The most popular posts list','My') )
        );
    }

    public function widget( $args, $instance ) {
        echo '<div class="widget last_posts">';
        if ( ! empty( $instance[ 'title' ] ) ) echo '<div class="title">'.$instance[ 'title' ].'</div>';
        if ( empty( $instance[ 'count' ] ) ) $instance[ 'count' ] = 5;
        $args = array('meta_key'=>'views','order_by'=>'meta_value_num','showposts'=>$instance[ 'count' ],'post__not_in'=>array());
            if (is_category()) {
                $cat_ID = get_query_var('cat');
                $args['cat'] = $cat_ID;
            }
            if (is_single()) {
                global $post;
                $args['post__not_in'] = array($post->ID);
                $cat = get_the_category($post->ID);
                $cat = array_shift($cat);
                $args['cat'] = $cat->term_id;
            }
        echo '<ul>';
        do {
            $the_query = new WP_Query($args);
            while ($the_query -> have_posts()) : $the_query -> the_post();
            $category = get_the_category();
            $category = array_shift($category);
            $args['post__not_in'][] = $the_query->ID; ?>
            <li>
                <div class="thumb" style="background-image:url(<?php $output = wp_get_attachment_image_src(get_post_thumbnail_id($the_query->ID), 'normal'); echo $output[0]; ?>)"></div>
                <a class="category" href="<?php echo get_category_link($category->term_id); ?>"><?php echo $category->name; ?></a> <span class="arial">&rarr;</span> <a class="post" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </li>
            <?php endwhile;
            
            $continue = false;
            $i = count($args['post__not_in']);
            if (is_single()) $i--;
            if ($i<$args['showposts']) {
                if (isset($args['cat'])) {
                    $cat = get_category((int)$args['cat'],false);
                    $args['cat'] = $cat->parent;
                    $args['showposts'] = $args['showposts'] - $i;
                    $continue = true;
                }
            }
        } while ($continue);
        echo '</ul></div>';
    }

    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        } else {
            $title = '';
        }
        if ( isset( $instance[ 'count' ] ) ) {
            $count = $instance[ 'count' ];
        } else {
            $count = 5;
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Header','My'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e('Count','My'); ?></label>
            <input id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" value="<?php echo esc_attr( $count ); ?>" type="number" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
        return $instance;
    }
}

function popular_posts_widget_load() {
    register_widget( 'popularPostsWidget' );
}
add_action( 'widgets_init', 'popular_posts_widget_load' );